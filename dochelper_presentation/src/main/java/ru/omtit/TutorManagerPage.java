package ru.omtit;

import org.apache.wicket.request.mapper.parameter.PageParameters;

public class TutorManagerPage extends MasterPage {
    public TutorManagerPage(PageParameters parameters) {
        super(parameters);
        replace(new TutorManagerPanel(CONTENT_ID));
    }
}
