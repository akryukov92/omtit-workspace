package ru.omtit;

import org.apache.wicket.request.mapper.parameter.PageParameters;

public class EventManagerPage extends MasterPage {
    public EventManagerPage(PageParameters parameters) {
        super(parameters);
        replace(new EventManagerPanel(CONTENT_ID));
    }
}
