package ru.omtit;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.request.mapper.parameter.PageParameters;

public class TPlanPrintPage extends WebPage {
    public TPlanPrintPage(final PageParameters parameters) {
        super(parameters);
        this.setStatelessHint(true);
    }
}
