package ru.omtit;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.request.mapper.parameter.PageParameters;

/**
 * Created by AKryukov on 07.08.2015.
 */
public class KTPlanPrintPage extends WebPage {
    public KTPlanPrintPage(final PageParameters parameters) {
        super(parameters);
        this.setStatelessHint(true);
    }
}
