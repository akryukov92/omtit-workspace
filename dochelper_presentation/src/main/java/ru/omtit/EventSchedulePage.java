package ru.omtit;

import org.apache.wicket.request.mapper.parameter.PageParameters;

public class EventSchedulePage extends MasterPage {
    public EventSchedulePage(PageParameters parameters) {
        super(parameters);
        replace(new EventSchedulePanel(CONTENT_ID));
    }
}
