package ru.omtit;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import java.util.Locale;

/**
 * Created by AKryukov on 21.07.2015.
 */
public class MasterPage extends WebPage {

    public static final String CONTENT_ID = "workspace";

    public MasterPage(final PageParameters parameters) {
        super(parameters);
        this.setStatelessHint(true);

        add(new Label(CONTENT_ID, "Content placeholder"));
    }
}
