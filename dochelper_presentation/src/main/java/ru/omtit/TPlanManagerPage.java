package ru.omtit;

import org.apache.wicket.request.mapper.parameter.PageParameters;

/**
 * Created by AKryukov on 27.07.2015.
 */
public class TPlanManagerPage extends MasterPage {
    public TPlanManagerPage(PageParameters parameters) {
        super(parameters);
        replace(new TPlanManagerPanel(CONTENT_ID));
    }
}
