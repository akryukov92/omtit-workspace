package ru.omtit;

import org.apache.wicket.request.mapper.parameter.PageParameters;

public class EPlanManagerPage extends MasterPage {
    public EPlanManagerPage(PageParameters parameters) {
        super(parameters);
        replace(new EPlanManagerPanel(CONTENT_ID));
    }
}
