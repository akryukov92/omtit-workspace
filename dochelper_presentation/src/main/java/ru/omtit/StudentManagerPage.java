package ru.omtit;

import org.apache.wicket.request.mapper.parameter.PageParameters;

public class StudentManagerPage extends MasterPage{
    public StudentManagerPage(PageParameters parameters) {
        super(parameters);
        replace(new StudentManagerPanel(CONTENT_ID));
    }
}
