package ru.omtit;

import org.apache.wicket.request.mapper.parameter.PageParameters;

public class GroupManagerPage extends MasterPage {
    public GroupManagerPage(PageParameters parameters) {
        super(parameters);
        replace(new GroupManagerPanel(CONTENT_ID));
    }
}
