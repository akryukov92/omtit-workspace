package ru.omtit;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.protocol.http.WebApplication;

/**
 * Application object for your web application.
 */
public class WicketApplication extends WebApplication
{
	/**
	 * @see org.apache.wicket.Application#getHomePage()
	 */
	@Override
	public Class<? extends WebPage> getHomePage()
	{
		return MasterPage.class;
	}

	/**
	 * @see org.apache.wicket.Application#init()
	 */
	@Override
	public void init()
	{
		super.init();
        mountPage("/eplans", EPlanManagerPage.class);
        mountPage("/tplans", TPlanManagerPage.class);
        mountPage("/tplan-print", TPlanPrintPage.class);
        mountPage("/ktplan-print", KTPlanPrintPage.class);
        mountPage("/groups", GroupManagerPage.class);
		mountPage("/tutors", TutorManagerPage.class);
		mountPackage("/events", EventManagerPage.class);
	}
}
