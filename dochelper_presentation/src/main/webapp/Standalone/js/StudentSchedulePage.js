var me;
function processCards (data) {
	var content = "";
	for (var i = 0; i < data.length; i++) {
		if (data[i].due != null) {
			content += "<div id='" + data[i].id + "'>" +
				"<div id='board" + data[i].idBoard + "'></div>" +
				"<div class='dueDate'>" + data[i].due + "</div>" +
				"<div class='activityName'>" + data[i].name + "</div>" +
			"</div>";
			var dateSplitter = /(\d{4})-(\d{2})-(\d{2})\w{1}(\d{2}):(\d{2})/g;
			var fragments = dateSplitter.exec(data[i].due);
			console.log(fragments);
			var tempDate = new Date(data[i].due);
			Trello.boards.get(data[i].idBoard, processBoard);
		}
	}
	document.getElementById("activities").innerHTML = content;
}
function processBoard (data) {
	document.getElementById("board" + data.id).innerHTML = data.name;
}
function onLoggedIn(data) {
	Trello.members.get("me", function(member) {
		me = member;
		document.getElementById("username").innerHTML = "�� ����� ��� " + member.fullName;
	});
	document.getElementById("loggedout").style.display = "none";
	document.getElementById("loggedin").style.display = "block";
	Trello.get("members/me/cards", processCards);
}
function login() {
	Trello.authorize({
		type: "popup",
		name: "custom api client",
		persist: true,
		interactive : true,
		scope: {
			read: true,
			write: true,
			account: false
		},
		success: onLoggedIn
	});
}
function logout() {
	Trello.deauthorize();
	document.getElementById("loggedout").style.display = "block";
	document.getElementById("username").innerHTML = "�� �����";
	document.getElementById("loggedin").style.display = "none";
}
function refresh() {
	Trello.get("members/me/cards", processCards);
}
function sayHello() {
	var helloInGroup = document.getElementById("helloInGroup").value;
	if (helloInGroup != "") {
		Trello.post("lists/55fbca20da4d8f8c68612f01/cards", {
			name: me.fullName,
			desc: "���� ������� � ������ " + helloInGroup,
			idMembers: me.id
			});
	}
}