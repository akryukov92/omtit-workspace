﻿var me;
var days = ["понедельник", "вторник", "среда", "четверг", "пятница", "суббота", "воскресение"];
var months = ["", "января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря"];

function prepareDate(date) {
	return days[date.getDay()] + " " + date.getUTCDate() + " " + months[date.getUTCMonth()] + " " + date.getUTCFullYear() + " в " + date.getHours() + ":" + date.getMinutes();
}
function processCards (data) {
	var content = "";
	for (var i = 0; i < data.length; i++) {
		//console.log(data[i]);
		if (data[i].due != null) {
			var dateSplitter = /(\d{4})-(\d{2})-(\d{2})\w{1}(\d{2}):(\d{2})/g;
			var fragments = dateSplitter.exec(data[i].due);
			//dateObj = new Date(year, month, date[, hours, minutes, seconds, ms] )
			var tempDate = new Date(fragments[1], fragments[2], fragments[3], fragments[4], fragments[5]);
			tempDate.setHours(tempDate.getHours() + 6);//From UTC
			
			var skip = false;
			for (var j = 0; j < data[i].labels.length; j++) {
				var label = data[i].labels[j];
				if (label.color == "green") {
					skip = true;
					break;
				}
			}
			if (skip) {
				break;
			}
			var element = document.getElementById("card" + data[i].id);
			if (element == null) {
				document.getElementById("board" + data[i].idBoard).innerHTML += "<div id='card" + data[i].id + "' class='activity'></div>";
				element = document.getElementById("card" + data[i].id);
			}
			content =
				"<div id='board" + data[i].idBoard + "' class='id'></div>" +
				"<div class='dueDate'>" + prepareDate(tempDate) + "</div>" +
				"<div class='activityName'>" + data[i].name + "</div>";
			element.innerHTML = content;
		}
	}
}
function processBoards (data) {
	for(var i = 0; i < data.length; i++) {
		var element = document.getElementById("group" + data[i].id);
		if (element == null) {
			document.getElementById("groups").innerHTML +=
				"<div class='group' id='group" + data[i].id + "'></div>";
			element = document.getElementById("group" + data[i].id);
		}
		var content =
			"<div class='groupName'>" + data[i].name + "</div>" +
			"<div id='board" + data[i].id + "'></div>";
			element.innerHTML = content;
		Trello.get("boards/" + data[i].id + "/cards", processCards);
	}
}
function refresh() {
	Trello.members.get("me", function(member) {
		me = member;
		document.getElementById("username").innerHTML = member.fullName;
	});
	Trello.get("members/me/boards/?filter=open", processBoards);
}
function onLoggedIn(data) {
	document.getElementById("loggedout").style.display = "none";
	document.getElementById("loggedin").style.display = "block";
	refresh();
}
function login() {
	Trello.authorize({
		type: "popup",
		name: "Расписание преподавателей",
		persist: true,
		interactive : true,
		scope: {
			read: true,
			write: true,
			account: false
		},
		success: onLoggedIn
	});
}
function logout() {
	Trello.deauthorize();
	document.getElementById("loggedout").style.display = "block";
	document.getElementById("username").innerHTML = "Вы вышли";
	document.getElementById("loggedin").style.display = "none";
	document.getElementById("groups").innerHTML = "";
}