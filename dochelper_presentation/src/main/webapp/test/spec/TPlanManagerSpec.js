describe("TPlanController", function() {
    beforeEach(function() {
        angular.module('ui.grid', []);
        angular.module('ui.grid.edit', []);
        angular.module('ui.grid.selection', []);
        module('TPlanManager');
    });

    describe('mock tPlanFactory', function() {
        var tPlanController, scope;
        var defaultTPlan, partLink1, partLink2, themeLink1, themeLink2, newPart;
        beforeEach(inject(function($controller, $rootScope,  unitFactory, tPlanFactory,linkFactory) {
            scope = $rootScope.$new();
            defaultTPlan = { eplanId: 11, disciplineId:17, discipline: { id:17, name:'', type:'DISCIPLINE', theory:0, practice:0, control:0, personal:0 }, position:0};
            partLink1 = { childId:37, parentId:17, child: { id:37, name:'', type:'PART', theory:0, practice:0, control:0, personal:0}, position: 0 };
            partLink2 = { childId:41, parentId:17, child: { id:41, name:'', type:'PART', theory:0, practice:0, control:0, personal:0}, position: 0 };
            themeLink1 = { childId:43, parentId:37, child: { id:43, name:'', type:'THEME', theory:0, practice:0, control:0, personal:0}, position: 0 };
            themeLink2 = { childId: 47, parentId:37, child: { id:47, name:'', type:'THEME', theory:0, practice:0, control:0, personal:0}, position: 0 };
            newPart = {childId: 53, parentId:defaultTPlan.discipline.id, child:{ id: 53, name:'', position:0, type:'PART', theory:0, practice:0, control:0, personal:0 }, position: 0 };
            spyOn(tPlanFactory, 'get').and.callFake(function(ePlanId, disciplineId) {
                return {
                    success: function(callback) {
                        callback({data: defaultTPlan})
                    }
                };
            });
            spyOn(linkFactory, 'list').and.callFake(function(parentId) {
                if (parentId == defaultTPlan.discipline.id) {
                    return {
                        success: function(callback) {
                            callback({data: [ partLink1, partLink2 ]})
                        }
                    }
                } else if (parentId == partLink1.child.id) {
                    return {
                        success: function(callback) {
                            callback({data: [ themeLink1, themeLink2 ]})
                        }
                    }
                }
            });
            spyOn(linkFactory, 'add').and.callFake(function(parentId) {
                if (parentId == defaultTPlan.discipline.id) {
                    return {
                        success: function(callback) {
                            callback({data:newPart.id})
                        }
                    }
                }
            });
            tPlanController = $controller('TPlanController', {$scope : scope, unitFactory: unitFactory, tPlanFactory: tPlanFactory, linkFactory: linkFactory});
        }));
        it('should set current tPlan and discipline at startup', function() {
            expect(scope.currentTPlan).toEqual(defaultTPlan);
            expect(scope.currentDiscipline).toEqual(defaultTPlan.discipline);
        });
        it('should load parts of current discipline on startup', function() {
            expect(scope.partLinks).toContain(partLink1);
            expect(scope.partLinks).toContain(partLink2);
        });
        //it('should add part with specific properties to current discipline', function() {
        //    scope.addPart();
        //    expect(scope.currentDiscipline.linksToChildren).toContain(newPart);
        //})
    });
});