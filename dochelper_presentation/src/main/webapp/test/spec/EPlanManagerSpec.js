describe("EPlanController", function() {
    beforeEach(function(){
        angular.module('ui.grid',[]);
        angular.module('ui.grid.edit', []);
        angular.module('ui.grid.selection', []);
        module('EPlanManager');
    });

    describe('check created link', function() {
        var scope;
        beforeEach(inject(function($controller, $rootScope, ePlanFactory, tPlanFactory, unitFactory) {
            scope = $rootScope.$new();
            ePlanController = $controller('EPlanController', {$scope : scope, ePlanFactory: ePlanFactory, tPlanFactory: tPlanFactory, unitFactory: unitFactory});
        }));
        it('should create empty link if no EPlan selected', function() {
            scope.selectedEPlan = {};
            scope.selectedTPlan = { eplanId: 1, disciplineId: 11, position:1, discipline:{ id: 11 } };
            scope.prepareTPlanLink();
            expect(scope.tPlanLink).toEqual("#");
        });
        it('should create empty link if no TPlan selected', function() {
            scope.selectedEPlan = { id: 19, yearBegin:2014, yearEnd:2015, speciality:230105 };
            scope.selectedTPlan = {};
            scope.prepareTPlanLink();
            expect(scope.tPlanLink).toEqual("#");
        });
        it('should create empty link if TPlan contain no discipline data', function() {
            scope.selectedEPlan = { id: 19, yearBegin:2014, yearEnd:2015, speciality:230105 };
            scope.selectedTPlan = { eplanId: 1, disciplineId: 11, position:1 };
            scope.prepareTPlanLink();
            expect(scope.tPlanLink).toEqual("#");
        });
        it('should create link using id of EPlan and discipline of TPlan', function() {
            scope.selectedEPlan = { id: 19, yearBegin:2014, yearEnd:2015, speciality:230105 };
            scope.selectedTPlan = { eplanId: 1, disciplineId: 11, position:1, discipline: { id: 11 } };
            scope.prepareTPlanLink();
            expect(scope.tPlanLink).toEqual("/tplans?eplanId=19&disciplineId=11");
        });
    });

    describe('mock ePlanFactory', function() {
        var ePlanController, scope;
        var ePlan1, ePlan2, tPlan1, tPlan2, tPlan3, newPlan, fakeEPlan;
        var factory;
        beforeEach(inject(function($controller, $rootScope, ePlanFactory, tPlanFactory, unitFactory) {
            scope = $rootScope.$new();
            fakeEPlan = { id:11, speciality: 123456, yearBegin:1998, yearEnd:1999, tPlans:[] };
            ePlan1 = { id:19, yearBegin:2014, yearEnd:2015, speciality:230105 };
            ePlan2 = { id:23, yearBegin:2015, yearEnd:2016, speciality:230105 };
            tPlan1 = { eplanId:fakeEPlan.id, disciplineId:1, position:1 };
            tPlan2 = { eplanId:fakeEPlan.id, disciplineId:2, position:2 };
            tPlan3 = { eplanId:fakeEPlan.id, disciplineId:3, position:3 };
            newPlan = { id:31, speciality:0, yearBegin:2015, yearEnd:2016 };
            spyOn(tPlanFactory, 'list').and.callFake(function(ePlanId) {
                return {
                    success: function(callback) { callback({result:0, data:[ tPlan1, tPlan2, tPlan3 ]}) }
                };
            });
            spyOn(ePlanFactory, 'list').and.callFake(function(){
                return {
                    success: function(callback) { callback({result:0, data:[ ePlan1, ePlan2 ]}) }
                };
            });
            spyOn(ePlanFactory, 'add').and.callFake(function(ePlan){
                return {
                    success: function(callback) { callback({result:0, data:newPlan.id})}
                }
            });
            spyOn(ePlanFactory, 'save').and.callFake(function(ePlan) {
                return {
                    success: function(callback) { callback({result:0, data:newPlan.id}) }
                }
            });
            spyOn(ePlanFactory, 'remove').and.callFake(function(ePlanId) {
                return {
                    success: function(callback) { callback({result:0, data:ePlanId})}
                }
            });
            factory = ePlanFactory;
            ePlanController = $controller('EPlanController', {$scope : scope, ePlanFactory: ePlanFactory, tPlanFactory: tPlanFactory, unitFactory: unitFactory});
        }));
        it('should load EPlans on startup', function() {
            expect(scope.ePlans.length).toEqual(2);
            expect(scope.ePlans).toContain(ePlan1);
            expect(scope.ePlans).toContain(ePlan2);
        });
        it('should select EPlan', function() {
            scope.handleEPlanSelectionChanged({isSelected:true, entity:ePlan1});
            expect(scope.selectedEPlan).toEqual(ePlan1);
            expect(scope.tPlanLink).toEqual("#");
        });
        it('should deselect EPlan', function() {
            scope.handleEPlanSelectionChanged({isSelected:true, entity:ePlan1});
            scope.handleEPlanSelectionChanged({isSelected:false});
            expect(isEmpty(scope.selectedEPlan)).toEqual(true);
            expect(scope.tPlanLink).toEqual("#");
        });
        it('should deselect EPlan and tPlan', function() {
            scope.handleEPlanSelectionChanged({isSelected:true, entity:ePlan1});
            scope.handleTPlanSelectionChanged({isSelected:true, entity:tPlan1});
            scope.handleEPlanSelectionChanged({isSelected:false});
            expect(isEmpty(scope.selectedEPlan)).toEqual(true);
            expect(typeof scope.selectedEPlan.tPlans).toEqual('undefined');
            expect(isEmpty(scope.selectedTPlan)).toEqual(true);
            expect(scope.tPlanLink).toEqual("#");
        });
        it('should remove selected ePlan from list', function() {
            scope.handleEPlanSelectionChanged({isSelected:true, entity:ePlan1});
            scope.deleteEPlan();
            expect(scope.ePlans).not.toContain(ePlan1);
            expect(isEmpty(scope.selectedEPlan)).toEqual(true);
            expect(typeof scope.selectedEPlan.tPlans).toEqual('undefined');
            expect(scope.tPlanLink).toEqual("#");
        });
        it('should remove selected ePlan and clear selected tPlan', function() {
            scope.handleEPlanSelectionChanged({isSelected:true, entity:ePlan1});
            scope.handleTPlanSelectionChanged({isSelected:true, entity:tPlan1});
            scope.deleteEPlan();
            expect(isEmpty(scope.selectedTPlan)).toEqual(true);
            expect(scope.tPlanLink).toEqual("#");
        });
        it('should fill list of tPlans by selection of ePlan ', function() {
            scope.handleEPlanSelectionChanged({isSelected: true, entity:fakeEPlan});
            expect(scope.selectedEPlan.tPlans).toContain(tPlan1);
            expect(scope.selectedEPlan.tPlans).toContain(tPlan2);
            expect(scope.selectedEPlan.tPlans).toContain(tPlan3);
            expect(scope.tPlanLink).toEqual("#");
        });
        it('should not call add if speciality length is less than 6 and greater than 10', function() {
            scope.newEPlan.speciality = "12345";
            scope.newEPlan.yearBegin = 2014;
            scope.newEPlan.yearEnd = 2015;
            scope.addEPlan();
            scope.newEPlan.speciality = "12345678901234567890";
            scope.newEPlan.yearBegin = 2014;
            scope.newEPlan.yearEnd = 2015;
            scope.addEPlan();
            expect(factory.add.calls.all().length).toEqual(0);
        });
        it('should not call add if yearBegin is not between 1995 and 2050', function() {
            scope.newEPlan.speciality = "123456";
            scope.newEPlan.yearBegin = 1994;
            scope.newEPlan.yearEnd = 2015;
            scope.addEPlan();
            scope.newEPlan.speciality = "123456";
            scope.newEPlan.yearBegin = 2051;
            scope.newEPlan.yearEnd = 2015;
            scope.addEPlan();
            expect(factory.add.calls.all().length).toEqual(0);
        });
        it('should not call add if yearEnd is not between 1995 and 2050', function() {
            scope.newEPlan.speciality = "123456";
            scope.newEPlan.yearBegin = 2015;
            scope.newEPlan.yearEnd = 1994;
            scope.addEPlan();
            scope.newEPlan.speciality = "123456";
            scope.newEPlan.yearBegin = 2015;
            scope.newEPlan.yearEnd = 2100;
            scope.addEPlan();
            expect(factory.add.calls.all().length).toEqual(0);
        });
        it('should not call add if yearBegin greater than yearEnd',function() {
            scope.newEPlan.speciality = "123456";
            scope.newEPlan.yearBegin = 2015;
            scope.newEPlan.yearEnd = 2014;
            scope.addEPlan();
            expect(factory.add.calls.all().length).toEqual(0);
        });
        it ('should not call add if any field is undefined', function() {
            scope.newEPlan.speciality = undefined;
            scope.newEPlan.yearBegin = 2015;
            scope.newEPlan.yearEnd = 2016;
            scope.addEPlan();
            scope.newEPlan.speciality = "123456";
            scope.newEPlan.yearBegin = undefined;
            scope.newEPlan.yearEnd = 2016;
            scope.addEPlan();
            scope.newEPlan.speciality = "123456";
            scope.newEPlan.yearBegin = 2015;
            scope.newEPlan.yearEnd = undefined;
            scope.addEPlan();
            expect(factory.add.calls.all().length).toEqual(0);
        });
        it('should call add if ePlan described properly', function() {
            scope.newEPlan.speciality = "123456";
            scope.newEPlan.yearBegin = 2015;
            scope.newEPlan.yearEnd = 2016;
            scope.addEPlan();
            expect(factory.add.calls.all().length).toEqual(1);
        });
        it('should put ePlan to list after add', function() {
            var lengthBefore = scope.ePlans.length;
            scope.newEPlan.speciality = "123456";
            scope.newEPlan.yearBegin = 2015;
            scope.newEPlan.yearEnd = 2016;
            scope.addEPlan();
            expect(scope.ePlans.length).toEqual(lengthBefore + 1);
        })
    });

    describe('mock tPlanFactory', function() {
        var ePlanController, scope;
        var tPlan1, tPlan2, tPlan3, fakeEPlan, newTPlan;
        var factory;
        beforeEach(inject(function($controller, $rootScope, ePlanFactory, tPlanFactory, unitFactory) {
            scope = $rootScope.$new();
            fakeEPlan = { id: 11, speciality: 123456, yearBegin:1998, yearEnd: 1999, tPlans:[] };
            tPlan1 = { eplanId: fakeEPlan.id, disciplineId: 1, position:1 };
            tPlan2 = { eplanId: fakeEPlan.id, disciplineId: 2, position:2 };
            tPlan3 = { eplanId: fakeEPlan.id, disciplineId: 3, position:3 };
            newTPlan = { discipline:{ id:17, name:'', type:'DISCIPLINE', theory:0, practice:0, control:0, personal:0 }, ePlanId: fakeEPlan.id, disciplineId:17, position:0 };
            spyOn(tPlanFactory, 'list').and.callFake(function(ePlanId) {
                return {
                    success: function(callback) { callback({result:0, data:[ tPlan1, tPlan2, tPlan3 ]}) }
                };
            });
            spyOn(tPlanFactory, 'remove').and.callFake(function(tPlan) {
                return {
                    success: function(callback) {
                        callback({result:0})
                    }
                };
            });
            spyOn(tPlanFactory,'add').and.callFake(function() {
                return {
                    success: function(callback) {
                        callback({result:0, data:newTPlan.discipline.id})
                    }
                };
            });
            factory = tPlanFactory;
            ePlanController = $controller('EPlanController', {$scope : scope, ePlanFactory: ePlanFactory, tPlanFactory: tPlanFactory, unitFactory: unitFactory});
        }));
        it('should select tPlan', function() {
            scope.handleTPlanSelectionChanged({isSelected:true, entity:tPlan1});
            expect(scope.selectedTPlan).toEqual(tPlan1);
            expect(scope.tPlanLink).toEqual("#");
        });
        it('should deselect tPlan', function() {
            scope.handleTPlanSelectionChanged({isSelected:false});
            expect(isEmpty(scope.selectedTPlan)).toEqual(true);
            expect(scope.tPlanLink).toEqual("#");
        });
        it('should delete selected tPlan contained in selected ePlan', function() {
            scope.handleEPlanSelectionChanged({isSelected: true, entity:fakeEPlan});
            scope.handleTPlanSelectionChanged({isSelected:true, entity:tPlan1});
            scope.deleteTPlan();
            expect(scope.selectedEPlan.tPlans).not.toContain(tPlan1);
            expect(scope.tPlanLink).toEqual("#");
        });
        it('should call add if discipline is described properly', function() {
            scope.handleEPlanSelectionChanged({isSelected:true, entity:fakeEPlan});
            scope.newDiscipline.name = "discname";
            scope.newDiscipline.theory = 1;
            scope.newDiscipline.practice = 2;
            scope.newDiscipline.personal = 3;
            scope.addTPlan();
            expect(factory.add.calls.all().length).toEqual(1);
        });
        it ('should not call add if EPlan is not selected', function() {
            scope.newDiscipline.name = "discname";
            scope.newDiscipline.theory = 1;
            scope.newDiscipline.practice = 2;
            scope.newDiscipline.personal = 3;
            scope.addTPlan();
            expect(factory.add.calls.all().length).toEqual(0);
        });
        it('should not call add if numerics are negative', function() {
            scope.handleEPlanSelectionChanged({isSelected:true, entity:fakeEPlan});
            scope.newDiscipline.name = "discname";
            scope.newDiscipline.theory = -1;
            scope.newDiscipline.practice = 2;
            scope.newDiscipline.personal = 3;
            scope.addTPlan();
            scope.newDiscipline.name = "discname";
            scope.newDiscipline.theory = 1;
            scope.newDiscipline.practice = -2;
            scope.newDiscipline.personal = 3;
            scope.addTPlan();
            scope.newDiscipline.name = "discname";
            scope.newDiscipline.theory = 1;
            scope.newDiscipline.practice = 2;
            scope.newDiscipline.personal = -3;
            scope.addTPlan();
            expect(factory.add.calls.all().length).toEqual(0);
        });
        it('should not call add if anything is undefined', function() {
            scope.handleEPlanSelectionChanged({isSelected:true, entity:fakeEPlan});
            scope.newDiscipline.name = undefined;
            scope.newDiscipline.theory = 1;
            scope.newDiscipline.practice = 2;
            scope.newDiscipline.personal = 3;
            scope.addTPlan();
            scope.newDiscipline.name = "discname";
            scope.newDiscipline.theory = undefined;
            scope.newDiscipline.practice = 2;
            scope.newDiscipline.personal = 3;
            scope.addTPlan();
            scope.newDiscipline.name = "discname";
            scope.newDiscipline.theory = 1;
            scope.newDiscipline.practice = undefined;
            scope.newDiscipline.personal = 3;
            scope.addTPlan();
            scope.newDiscipline.name = "discname";
            scope.newDiscipline.theory = 1;
            scope.newDiscipline.practice = 2;
            scope.newDiscipline.personal = undefined;
            scope.addTPlan();
            expect(factory.add.calls.all().length).toEqual(0);
        });
    });
});