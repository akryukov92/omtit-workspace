angular.module('StudentManager', ['ui.grid','ui.grid.selection'])
.factory('studentFactory', dataSources.studentFactory)
.factory('studentLinkFactory', dataSources.studentLinkFactory)
.controller('StudentController', function($scope, studentFactory, studentLinkFactory){
    $scope.selectedStudent = {};
    $scope.newStudent = {};
    $scope.students = [];

    $scope.studentGridOptions = { data: 'students',
        enableRowSelection:true,
        enableRowHeaderSelection:false,
        multiSelect:false,
        enableCellEdit:true,
        columnDefs:[
            {field:'name', displayName:'ФИО', width:'*'},
            {field:'birthDate', displayName:'Дата рождения', width:"50", type:'date'},
            {field:'address', displayName:'Адрес', width:'*'},
            {field:'education', displayName:'Образование', width:'*'},
            {field:'sourceOrg', displayName:'Наименование центра занятости', width:'*'}
        ]};

    $scope.historyGridOptions = { data: 'selectedStudent.links',
        enableRowSelection:true,
        enableRowHeaderSelection:false,
        multiSelect:false,
        enableCellEdit:true,
        columnDefs:[
            {field:'group.name', displayName:'Номер группы', width:'*'},
            {field:'since', displayName:'Дата начала обучения', width:'50'},
            {field:'till', displayName:'Дата окончания обучения', width:'50'},
            {field:'order_id', displayName:'Приказ', width:'50'}
        ]};

    $scope.studentGridOptions.onRegisterApi = function(gridApi) {
        $scope.studentGridApi = gridApi;
        $scope.studentGridApi.selection.on.rowSelectionChanged($scope, function(row) {
            if (row.isSelected) {
                $scope.selectedStudent = row.entity;
                $scope.getStudentLinks(row.entity);
            } else {
                $scope.selectedStudent = {};
            }
        });
        $scope.studentGridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
            $scope.saveStudent(rowEntity);
        });
    };

    $scope.saveStudent = function(student) {
        studentFactory.save(student).success(function(data) {
            if (data.result != 0) {
                return;
            }
        });
    };

    $scope.addStudent = function() {
        if (isEmpty($scope.selectedStudent)) {
            return;
        }
        var student = {
            id: -1,
            name: $scope.newStudent.name,
            birthDate: $scope.newStudent.birthDate,
            address: $scope.newStudent.address,
            education: $scope.newStudent.education,
            sourceOrg: $scope.newStudent.sourceOrg
        };
        studentFactory.add(student).success(function(data) {
            if (data.result != 0) {
                return;
            }
            student.id = data.data;
            $scope.students.push(student);
        });
    };

    $scope.getStudentLinks = function(student) {
        if (isEmpty(student)) {
            return;
        }
        studentLinkFactory.get(student.id).success(function(data) {
            if (data.result != 0) {
                return;
            }
            student.links = data.data;
        });
    };
});