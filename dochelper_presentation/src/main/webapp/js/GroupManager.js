angular.module('GroupManager', ['ui.grid', 'ui.grid.edit', 'ui.grid.selection'])
.value('version', 'v1.0.1')
.factory('ePlanFactory', dataSources.EPlanFactory)
.factory('groupFactory', dataSources.groupFactory)
.controller('GroupController', function GroupController($scope, ePlanFactory, groupFactory) {
    $scope.ePlans = [];
    $scope.selectedEPlan = {};
    $scope.selectedGroup = {};
    $scope.groups = [];
    $scope.newGroup = {};
    $scope.newName = "";

    $scope.ePlanGridOptions = { data : 'ePlans',
        enableRowSelection:true,
        enableRowHeaderSelection:false,
        multiSelect:false,
        enableCellEdit:false,
        columnDefs:[
            {field:'speciality', displayName: 'Специальность', width:"*"},
            {field:'yearBegin',displayName:'Начало обучения', width:150, type:'number'},
            {field:'yearEnd', displayName:'Окончание обучения', width:150, type:'number'}
        ]};

    $scope.groupGridOptions = { data : 'groups',
        enableRowSelection: true,
        enableRowHeaderSelection: false,
        multiSelect:false,
        enableCellEdit:false,
        columnDefs: [
            {field:'name', displayName:'Код группы', width:"*"},
            {field:'ePlan.speciality', displayName: 'Специальность', width:"*"},
            {field:'ePlan.yearBegin', displayName:'Начало обучения', width:150, type:'number'},
            {field:'ePlan.yearEnd', displayName:'Окончание обучения', width:150, type:'number'}
        ]};

    $scope.ePlanGridOptions.onRegisterApi = function(gridApi) {
        $scope.ePlanGridApi = gridApi;
        $scope.ePlanGridApi.selection.on.rowSelectionChanged($scope, function(row) {
            if (row.isSelected) {
                $scope.selectedEPlan = row.entity;
            } else {
                $scope.selectedEPlan = {};
            }
        });
    };

    $scope.groupGridOptions.onRegisterApi = function(gridApi) {
        $scope.groupGridApi = gridApi;
        $scope.groupGridApi.selection.on.rowSelectionChanged($scope, function(row) {
            if (row.isSelected) {
                $scope.selectGroup(row.entity);
            } else {
                $scope.selectGroup({});
            }
        });
    };

    $scope.selectGroup = function(group) {
        $scope.selectedGroup = group;
    };

    $scope.addGroup = function() {
        if (isEmpty($scope.selectedEPlan)) {
            return;
        }
        var ePlan = $scope.selectedEPlan;
        var newGroup = {
            id: -1,
            name: $scope.newGroup.name,
            eplanId: ePlan.id,
            ePlan: ePlan
        };
        groupFactory.add(newGroup).success(function(data) {
            if (data.result != 0) {
                return;
            }
            newGroup.id = data.data;
            $scope.groups.push(newGroup);
        });
    };

    $scope.deleteGroup = function() {
        if (isEmpty($scope.selectedGroup)) {
            return;
        }
        if (!confirm('Вы уверены, что хотите удалить группу')) {
            return;
        }
        var group = $scope.selectedGroup;
        groupFactory.delete(group.id).success(function(data) {
            if (data.result != 0) {
                return;
            }
            for (var i =0; i < $scope.groups.length; i++) {
                if ($scope.groups[i].id == group.id) {
                    $scope.groups.splice(i,1);
                    break;
                }
            }
            $scope.refreshEPlans();
        });
    };

    $scope.renameGroup = function() {
        if (isEmpty($scope.selectedGroup)) {
            return;
        }
        var group = $scope.selectedGroup;
        group.name = $scope.newName;
        groupFactory.update(group).success(function(data) {
            if (data.result != 0) {
                return;
            }
        });
    };

    $scope.refreshEPlans = function () {
        ePlanFactory.list().success(function(data){
            if (data.result != 0) {
                return;
            }
            $scope.ePlans = data.data;
        });
        $scope.selectedEPlan = {};
    };

    $scope.getEPlanForGroup = function(group) {
        ePlanFactory.get(group.eplanId).success(function(data) {
            if (data.result != 0) {
                return;
            }
            group.ePlan = data.data;
        });
    };

    $scope.refreshGroups = function() {
        groupFactory.list().success(function(data) {
            if (data.result != 0) {
                return;
            }
            $scope.groups = data.data;
            for(var i = 0; i < $scope.groups.length; i++) {
                $scope.getEPlanForGroup($scope.groups[i]);
            }
        });
        $scope.selectedGroup = {};
    };

    $scope.refreshGroups();
    $scope.refreshEPlans();
});