var dataSources = dataSources || {};

dataSources.EPlanFactory = function($http) {
    var EPlanURL = "http://api.omtit.ru:8080/services/eplan";
    return {
        list: function() {
            return $http.get(EPlanURL);
        },
        save: function(ePlan) {
            return $http.post(EPlanURL + "/update", ePlan);
        },
        remove: function(ePlanId) {
            return $http.post(EPlanURL + "/delete", ePlanId);
        },
        add: function(ePlan) {
            return $http.post(EPlanURL + "/add", ePlan);
        },
        get: function(ePlanId) {
            return $http.get(EPlanURL + "/item?id=" + ePlanId);
        }
    }
};

dataSources.TPlanFactory = function($http) {
    var TPlanURL = "http://api.omtit.ru:8080/services/tplan";
    return {
        list: function(ePlanId) {
            return $http.get(TPlanURL + "?eplanId=" + ePlanId);
        },
        get: function(ePlanId, disciplineId) {
            return $http.get(TPlanURL + "/item?eplanId=" + ePlanId + "&disciplineId=" + disciplineId);
        },
        add: function(tPlan) {
            return $http.post(TPlanURL + "/add", tPlan);
        },
        remove: function(tPlan) {
            return $http.post(TPlanURL + "/delete", tPlan)
        }
    }
};

dataSources.unitFactory = function($http) {
    var UnitURL = "http://api.omtit.ru:8080/services/unit";
    return {
        save: function(unit){
            return $http.post(UnitURL + "/update", unit);
        }
    }
};

dataSources.linkFactory = function($http) {
    var LinkURL = "http://api.omtit.ru:8080/services/unitlink";
    return {
        list: function(parentId) {
            return $http.get(LinkURL + "?parentId=" + parentId)
        },
        add: function(link) {
            return $http.post(LinkURL + "/add", link);
        },
        remove: function(link) {
            return $http.post(LinkURL + "/delete", link);
        },
        update: function(link) {
            return $http.post(LinkURL + "/update", link);
        }
    }
};

dataSources.groupFactory = function($http) {
    var LinkURL = "http://api.omtit.ru:8080/services/group";
    return {
        list: function() {
            return $http.get(LinkURL);
        },
        listByEPlan: function(ePlanId) {
            return $http.get(LinkURL + "/list?eplanId=" + ePlanId)
        },
        add: function(group) {
            return $http.post(LinkURL + "/add", group);
        },
        update: function(group) {
            return $http.post(LinkURL + "/update", group);
        },
        delete: function(groupId) {
            return $http.post(LinkURL + "/delete", groupId);
        }
    }
};