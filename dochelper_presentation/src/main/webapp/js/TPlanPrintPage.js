angular.module('TPlanPrint',[])
.factory('ePlanFactory', dataSources.EPlanFactory)
.factory('tPlanFactory', dataSources.TPlanFactory)
.factory('unitFactory', dataSources.unitFactory)
.factory('linkFactory', dataSources.linkFactory)
.controller('TPlanPrintController', function ($scope, unitFactory, ePlanFactory, tPlanFactory, linkFactory) {
    $scope.ePlanId = getUrlParameter('eplanId');
    $scope.disciplineId = getUrlParameter('disciplineId');
    $scope.currentTPlan = {};
    $scope.currentEPlan = {};
    $scope.currentDiscipline = {};
    $scope.partLinks = [];
    $scope.tableRows = [];

    $scope.getEPlan = function() {
        ePlanFactory.get($scope.ePlanId).success(function(data) {
            if (data.result != 0) {
                return;
            }
            $scope.currentEPlan = data.data;
        });
    };

    $scope.getTPlan = function() {
        tPlanFactory.get($scope.ePlanId, $scope.disciplineId).success(function(data) {
            if (data.result != 0) {
                return;
            }
            $scope.currentTPlan = data.data;
            $scope.currentDiscipline = $scope.currentTPlan.discipline;
            document.title = $scope.currentDiscipline.name;
            $scope.currentDiscipline.total = $scope.currentDiscipline.theory + $scope.currentDiscipline.control + $scope.currentDiscipline.practice;
            $scope.currentDiscipline.idle = $scope.currentDiscipline.theory + $scope.currentDiscipline.control;
            $scope.refreshParts();
        })
    };

    $scope.refreshParts = function() {
        linkFactory.list($scope.currentDiscipline.id).success(function(data) {
            if (data.result != 0) {
                return;
            }
            $scope.partlinks = data.data;
            for (var i = 0; i < $scope.partlinks.length; i++) {
                var element = $scope.partlinks[i];
                $scope.getLinksToThemes(element);
            }
        });
    };

    $scope.getLinksToThemes = function(partLink) {
        var part = partLink.child;
        var type = 'none';
        linkFactory.list(partLink.child.id).success(function(data){
            if (data.result != 0) {
                return;
            }
            partLink.child.linksToChildren = data.data;
            var themes = [];
            for (var j = 0; j < data.data.length; j++) {
                var element = data.data[j].child;
                themes.push({
                    name: element.name,
                    theory: element.theory,
                    practice: element.practice,
                    idle: element.theory + element.control,
                    total: element.theory + element.control + element.practice,
                    position: data.data[j].position
                })
            }
            if (themes.length > 0){
                type = 'full-part';
            } else {
                type = 'empty-part';
            }
            $scope.tableRows.push({
                name: part.name,
                total: part.theory + part.control + part.practice,
                idle: part.theory + part.control,
                practice: part.practice,
                themes: themes,
                type: type,
                position: partLink.position
            });
        });
    };

    $scope.getEPlan();
    $scope.getTPlan();
});