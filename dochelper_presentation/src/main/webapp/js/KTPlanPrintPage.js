angular.module('KTPlanPrint', [])
.factory('ePlanFactory', dataSources.EPlanFactory)
.factory('tPlanFactory', dataSources.TPlanFactory)
.factory('unitFactory', dataSources.unitFactory)
.factory('linkFactory', dataSources.linkFactory)
.controller('KTPlanPrintController', function($scope, unitFactory, ePlanFactory, tPlanFactory, linkFactory) {
    $scope.ePlanId = getUrlParameter('eplanId');
    $scope.disciplineId = getUrlParameter('disciplineId');
    $scope.currentTPlan = {};
    $scope.currentEPlan = {};
    $scope.currentDiscipline = {};
    $scope.partLinks = [];
    $scope.tableRows = [];

    $scope.getEPlan = function() {
        ePlanFactory.get($scope.ePlanId).success(function(data) {
            if (data.result != 0) {
                return;
            }
            $scope.currentEPlan = data.data;
            $scope.getTPlan();
        });
    };

    $scope.getTPlan = function() {
        tPlanFactory.get($scope.ePlanId, $scope.disciplineId).success(function(data) {
            if (data.result != 0) {
                return;
            }
            $scope.currentTPlan = data.data;
            $scope.currentDiscipline = $scope.currentTPlan.discipline;
            document.title = $scope.currentDiscipline.name;
            $scope.currentDiscipline.total = $scope.currentDiscipline.theory + $scope.currentDiscipline.control + $scope.currentDiscipline.practice;
            $scope.currentDiscipline.idle = $scope.currentDiscipline.theory + $scope.currentDiscipline.control;
            $scope.refreshParts();
        });
    };

    $scope.refreshParts = function() {
        linkFactory.list($scope.currentDiscipline.id).success(function(data) {
            if (data.result != 0) {
                return;
            }
            $scope.partLinks = data.data;
            for (var i = 0; i < $scope.partLinks.length; i++) {
                $scope.getLinksToThemes($scope.partLinks[i]);
            }
        });
    };

    $scope.getLinksToThemes = function(partLink) {
        linkFactory.list(partLink.child.id).success(function(data){
            if (data.result != 0) {
                return;
            }
            partLink.child.linksToChildren = data.data;
            var themeLinks = partLink.child.linksToChildren;
            for (var j = 0; j < themeLinks.length; j++){
                $scope.getLinksToActivities(themeLinks[j], partLink);
            }
            if (themeLinks.length == 0) {
                partLink.filled = true;
            }
        });
    };

    $scope.getLinksToActivities = function(themeLink, partLink) {
        linkFactory.list(themeLink.child.id).success(function(data) {
            if (data.result != 0) {
                themeLink.filled = true;
                return;
            }
            themeLink.child.linksToChildren = data.data;
            themeLink.filled = true;
            $scope.updatePartFilledStatus(partLink);
            if ($scope.isTreeFilled()) {
                $scope.buildTableData();
            }
        });
    };

    $scope.updatePartFilledStatus = function(partLink) {
        for (var pi = 0; pi < partLink.child.linksToChildren.length; pi++) {
            var element = partLink.child.linksToChildren[pi];
            if (typeof element.filled == 'undefined') {
                return;
            }
        }
        partLink.filled = true;
    };

    $scope.isTreeFilled = function() {
        for (var pi = 0; pi < $scope.partLinks.length; pi ++) {
            if (typeof $scope.partLinks[pi].filled == 'undefined') {
                return false;
            }
        }
        return true;
    };

    $scope.constructPart = function (part, link, type) {
        return {
            type: type,
            name: part.name,
            items:[],
            total: part.theory + part.practice + part.control,
            practice: part.practice,
            position:  link.position
        };
    };

    $scope.constructUnit = function(unit, link, index, type){
        return {
            type: type,
            index: index,
            name: unit.name,
            total: unit.theory + unit.practice + unit.control,
            practice: unit.practice,
            position: index
        };
    };

    $scope.buildTableData = function() {
        var globalIndex = 1;
        $scope.partLinks.sort(function(a,b) {
            return a.position - b.position;
        });
        for (var pi = 0; pi < $scope.partLinks.length; pi++) {
            var tempPart = $scope.partLinks[pi].child;
            if (tempPart.linksToChildren.length == 0) {
                $scope.tableRows.push($scope.constructPart(tempPart, $scope.partLinks[pi], 'empty-part'));
            } else {
                var partItem = $scope.constructPart(tempPart, $scope.partLinks[pi], 'full-part');
                for (var ti = 0; ti < tempPart.linksToChildren.length; ti++) {
                    var tempTheme = tempPart.linksToChildren[ti].child;
                    if (tempTheme.linksToChildren.length == 0) {
                        partItem.items.push($scope.constructUnit(tempTheme, tempPart.linksToChildren[ti], globalIndex, 'empty-theme'));
                        globalIndex++;
                    } else {
                        partItem.items.push($scope.constructUnit(tempTheme, tempPart.linksToChildren[ti], globalIndex, 'full-theme'));
                        globalIndex++;
                        for (var ai = 0; ai < tempTheme.linksToChildren.length; ai++) {
                            var tempActivity = tempTheme.linksToChildren[ai].child;
                            partItem.items.push($scope.constructUnit(tempActivity, tempTheme.linksToChildren[ai], globalIndex, 'activity'));
                            globalIndex++;
                        }
                    }
                }
                $scope.tableRows.push(partItem);
            }
        }
    };

    $scope.getEPlan();
});