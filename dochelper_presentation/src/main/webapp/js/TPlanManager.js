angular.module('TPlanManager', ['ui.grid', 'ui.grid.edit', 'ui.grid.selection'])
.factory('tPlanFactory', dataSources.TPlanFactory)
.factory('unitFactory', dataSources.unitFactory)
.factory('linkFactory', dataSources.linkFactory)
.controller('TPlanController', function($scope, unitFactory, tPlanFactory, linkFactory) {
    $scope.eplanId = getUrlParameter('eplanId');
    $scope.disciplineId = getUrlParameter('disciplineId');
    $scope.currentTPlan = {};
    $scope.currentDiscipline = {};
    $scope.partLinks = [];
    $scope.selectedPart = {};
    $scope.selectedTheme = {};
    $scope.selectedActivity = {};
    $scope.newPart = {};
    $scope.newTheme = {};
    $scope.newActivity = {};

    var sortByPosition = function(a,b) {
        return a.position - b.position;
    };

    $scope.partGridOptions = { data: 'partLinks',
        enableRowSelection:true,
        enableRowHeaderSelection:false,
        multiSelect:false,
        enableCellEdit:true,
        columnDefs:[
            {field:'position', displayName:'№ п/п', width:50, type:'number', sortFn: sortByPosition},
            {field:'child.name', displayName:'Наименование', width:"*"},
            {field:'child.theory', displayName:'Теории', width:50, type:'number'},
            {field:'child.practice', displayName:'Практики', width:50, type:'number'},
            {field:'child.personal', displayName:'СРС', width:50, type:'number'}
        ]};

    $scope.themeGridOptions = { data:'selectedPart.child.linksToChildren',
        enableRowSelection:true,
        enableRowHeaderSelection:false,
        multiSelect:false,
        enableCellEdit:true,
        columnDefs:[
            {field:'position', displayName:'№ п/п', width:50, type:'number', sortFn: sortByPosition},
            {field:'child.name', displayName:'Наименование', width:"*"},
            {field:'child.theory', displayName:'Теории', width:50, type:'number'},
            {field:'child.practice', displayName:'Практики', width:50, type:'number'},
            {field:'child.personal', displayName:'СРС', width:50, type:'number'}
        ]};

    $scope.activityGridOptions = { data: 'selectedTheme.child.linksToChildren',
        enableRowSelection:true,
        enableRowHeaderSelection:false,
        multiSelect:false,
        enableCellEdit:true,
        columnDefs:[
            {field:'position', displayName:'№ п/п', width:50, type:'number', sortFn: sortByPosition},
            {field:'child.name', displayName:'Наименование', width:"*"},
            {field:'child.theory', displayName:'Теории', width:50, type:'number'},
            {field:'child.practice', displayName:'Практики', width:50, type:'number'},
            {field:'child.personal', displayName:'СРС', width:50, type:'number'},
            {field:'child.description', displayName:'Хар-ка', width:50}
        ]};

    $scope.partGridOptions.onRegisterApi = function(gridApi) {
        $scope.partGridApi = gridApi;
        $scope.partGridApi.selection.on.rowSelectionChanged($scope, function(row) {
            if (row.isSelected) {
                $scope.selectedPart = row.entity;
                $scope.getLinksToThemes(row.entity.id);
            } else {
                $scope.selectedPart = {};
            }
        });
        $scope.partGridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
            unitFactory.save(rowEntity.child).success(function(data){
                if (data.result != 0) {
                    return;
                }
                rowEntity.child.id = data.data;
            });
            linkFactory.update(rowEntity).success(function(data){
                if (data.result != 0) {
                    return;
                }
            });
            $scope.$apply();
        });
    };

    $scope.themeGridOptions.onRegisterApi = function(gridApi) {
        $scope.themeGridApi = gridApi;
        $scope.themeGridApi.selection.on.rowSelectionChanged($scope, function(row) {
            if (row.isSelected) {
                $scope.selectedTheme = row.entity;
                $scope.getLinksToActivities(row.entity.id);
            } else {
                $scope.selectedTheme = {};
            }
        });
        $scope.themeGridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
            unitFactory.save(rowEntity.child).success(function(data){
                if (data.result != 0) {
                    return;
                }
                rowEntity.child.id = data.data;
            });
            linkFactory.update(rowEntity).success(function(data){
                if (data.result != 0) {
                    return;
                }
            });
            $scope.$apply();
        });
    };

    $scope.activityGridOptions.onRegisterApi = function(gridApi) {
        $scope.activityGridApi = gridApi;
        $scope.activityGridApi.selection.on.rowSelectionChanged($scope, function(row) {
            if (row.isSelected) {
                $scope.selectedActivity = row.entity;
            } else {
                $scope.selectedActivity = {};
            }
        });
        $scope.activityGridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
            unitFactory.save(rowEntity.child).success(function(data){
                if(data.result != 0) {
                    return;
                }
                rowEntity.child.id = data.data;
            });
            linkFactory.update(rowEntity).success(function(data){
                if (data.result != 0) {
                    return;
                }
            });
            $scope.$apply();
        });
    };

    $scope.getTPlan = function() {
        tPlanFactory.get($scope.eplanId, $scope.disciplineId).success(function(data) {
            if (data.result != 0) {
                return;
            }
            $scope.currentTPlan = data.data;
            $scope.currentDiscipline = $scope.currentTPlan.discipline;
            document.title = $scope.currentDiscipline.name;
            $scope.refreshParts();
        });
    };

    $scope.refreshParts = function() {
        linkFactory.list($scope.currentDiscipline.id).success(function(data) {
            if (data.result != 0) {
                return;
            }
            $scope.partLinks = data.data;
        });
    };

    $scope.addPart = function() {
        if (isEmpty($scope.currentDiscipline)) {
            return;
        }
        var part = {
            id: -1,
            name: $scope.newPart.name,
            position: 0,
            type: 'PART',
            theory: $scope.newPart.theory,
            practice: $scope.newPart.practice,
            control: $scope.newPart.control,
            personal: $scope.newPart.personal
        };
        var newLink = { child: part, parentId: $scope.currentDiscipline.id, position: 0 };
        linkFactory.add(newLink).success(function(data) {
            if (data.result != 0) {
                return;
            }
            newLink.child.id = data.data;
            newLink.childId = data.data;
            $scope.partLinks.push(newLink);
        });
    };

    $scope.deletePart = function() {
        if (isEmpty($scope.selectedPart)) {
            return;
        }
        var partLink = $scope.selectedPart;
        partLink.parentId = $scope.currentDiscipline.id;
        linkFactory.remove(partLink).success(function(data) {
            if (data.result != 0) {
                return;
            }
            for(var i = 0; i < $scope.partLinks.length; i++) {
                if ($scope.partLinks[i].child.id == partLink.child.id) {
                    $scope.partLinks.splice(i,1);
                    break;
                }
            }
        });
    };

    $scope.addTheme = function() {
        if (isEmpty($scope.selectedPart)) {
            return;
        }
        var partLink = $scope.selectedPart;
        var theme = {
            id: -1,
            name: $scope.newTheme.name,
            type: 'THEME',
            theory: $scope.newTheme.theory,
            practice: $scope.newTheme.practice,
            control: $scope.newTheme.control,
            personal: $scope.newTheme.personal
        };
        var newLink = { child:theme, parentId:partLink.child.id, position:0 };
        linkFactory.add(newLink).success(function(data) {
            if (data.result != 0) {
                return;
            }
            newLink.child.id = data.data;
            partLink.child.linksToChildren.push(newLink);
        });
    };

    $scope.deleteTheme = function() {
        if (isEmpty($scope.selectedTheme) || isEmpty($scope.selectedPart)) {
            return;
        }
        var partLink = $scope.selectedPart;
        var themeLink = $scope.selectedTheme;
        linkFactory.remove(themeLink).success(function(data) {
            if (data.result != 0) {
                return;
            }
            for (var i = 0; i < partLink.child.linksToChildren.length; i++) {
                if (partLink.child.linksToChildren[i].child.id == themeLink.child.id) {
                    partLink.child.linksToChildren.splice(i,1);
                    break;
                }
            }
        });
    };

    $scope.getLinksToThemes = function() {
        if (isEmpty($scope.selectedPart)) {
            return;
        }
        var partLink = $scope.selectedPart;
        linkFactory.list(partLink.child.id).success(function(data) {
            if (data.result != 0) {
                return;
            }
            partLink.child.linksToChildren = data.data;
        });
    };

    $scope.getLinksToActivities = function() {
        if (isEmpty($scope.selectedTheme)) {
            return;
        }
        var themeLink = $scope.selectedTheme;
        linkFactory.list(themeLink.child.id).success(function (data) {
            if (data.result != 0) {
                return;
            }
            themeLink.child.linksToChildren = data.data;
        });
    };

    $scope.deleteActivity = function() {
        if (isEmpty($scope.selectedTheme) || isEmpty($scope.selectedActivity)) {
            return;
        }
        var themeLink = $scope.selectedTheme;
        var activityLink = $scope.selectedActivity;
        linkFactory.remove(activityLink).success(function(data) {
            if(data.result != 0) {
                return;
            }
            for (var i = 0; i < themeLink.child.linksToChildren.length; i++){
                if(themeLink.child.linksToChildren[i].child.id == activityLink.child.id) {
                    themeLink.child.linksToChildren.splice(i,1);
                    break;
                }
            }
        })
    };

    $scope.addActivity = function(theory, practice, control) {
        if(isEmpty($scope.selectedTheme)) {
            return;
        }
        var themeLink = $scope.selectedTheme;
        var activity = {
            id: -1,
            name: $scope.newActivity.name,
            type:'ACTIVITY',
            theory:theory,
            practice:practice,
            control:control,
            personal:0
        };
        var newLink = {
            child: activity,
            parentId: themeLink.child.id,
            position:0
        };
        linkFactory.add(newLink).success(function(data){
            if (data.result != 0) {
                return;
            }
            newLink.child.id = data.data;
            if (typeof themeLink.child.linksToChildren == ' undefined') {
                themeLink.child.linksToChildren = [];
            }
            themeLink.child.linksToChildren.push(newLink);
        });
    };

    $scope.getTPlan();
});