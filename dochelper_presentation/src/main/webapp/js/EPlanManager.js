angular.module('EPlanManager',['ui.grid', 'ui.grid.edit', 'ui.grid.selection'])
.value('version','v1.0.1')
.factory('ePlanFactory', dataSources.EPlanFactory)
.factory('tPlanFactory', dataSources.TPlanFactory)
.factory('unitFactory', dataSources.unitFactory)
.controller('EPlanController', function EPlanController($scope, ePlanFactory, tPlanFactory, unitFactory) {
    $scope.ePlans = [];
    $scope.selectedEPlan = {};
    $scope.selectedTPlan = {};
    $scope.newEPlan = {};
    $scope.newDiscipline = {};
    $scope.tPlanLink = "#";

    $scope.ePlanGridOptions = { data : 'ePlans',
        enableRowSelection:true,
        enableRowHeaderSelection:false,
        multiSelect:false,
        enableCellEdit:true,
        columnDefs:[
            {field:'speciality', displayName: 'Speciality', width:"*"},
            {field:'yearBegin',displayName:'Year Begin', width:150, type:'number'},
            {field:'yearEnd', displayName:'Year End', width:150, type:'number'}
        ]};

    $scope.tPlanGridOptions = { data : 'selectedEPlan.tPlans',
        enableRowSelection:true,
        enableRowHeaderSelection:false,
        multiSelect:false,
        enableCellEdit:true,
        columnDefs:[
            {field:'discipline.name', displayName:'Name', width:"*"},
            {field:'discipline.theory', displayName:'Theory', width:50, type:'number'},
            {field:'discipline.practice', displayName:'Practice', width:50, type:'number'},
            {field:'discipline.personal', displayName:'Personal', width:50, type:'number'}
        ]
    };

    $scope.ePlanGridOptions.onRegisterApi = function(gridApi) {
        $scope.ePlanGridApi = gridApi;
        $scope.ePlanGridApi.selection.on.rowSelectionChanged($scope, $scope.handleEPlanSelectionChanged);
        $scope.ePlanGridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
            $scope.saveEPlan(rowEntity);
        });
    };

    $scope.tPlanGridOptions.onRegisterApi = function(gridApi) {
        $scope.tPlanGridApi = gridApi;
        $scope.tPlanGridApi.selection.on.rowSelectionChanged($scope, $scope.handleTPlanSelectionChanged);
        $scope.tPlanGridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue,oldValue) {
            $scope.saveUnit(rowEntity.discipline);
        });
    };

    $scope.handleEPlanSelectionChanged = function(row) {
        if (row.isSelected) {
            var ePlan = row.entity;
            $scope.selectedEPlan = ePlan;
            tPlanFactory.list(ePlan.id).success(function(data) {
                if (data.result != 0) {
                    return;
                }
                ePlan.tPlans = data.data;
            });
        } else {
            $scope.selectedEPlan = {};
            $scope.selectedTPlan = [];
        }
        $scope.prepareTPlanLink();
    };

    $scope.handleTPlanSelectionChanged = function(row) {
        if (row.isSelected) {
            $scope.selectedTPlan = row.entity;
        } else {
            $scope.selectedTPlan = {};
        }
        $scope.prepareTPlanLink();
    };

    $scope.prepareTPlanLink = function() {
        if (isEmpty($scope.selectedTPlan)) {
            $scope.tPlanLink = "#";
            return;
        }
        if (isEmpty($scope.selectedEPlan)) {
            $scope.tPlanLink = "#";
            return;
        }
        if (isEmpty($scope.selectedTPlan.discipline)) {
            $scope.tPlanLink = "#";
            return;
        }
        if (typeof $scope.selectedTPlan.discipline.id == 'undefined') {
            $scope.tPlanLink = "#";
            return;
        }
        if (typeof $scope.selectedEPlan.id == 'undefined') {
            $scope.tPlanLink = "#";
            return;
        }
        $scope.tPlanLink = "?eplanId=" + $scope.selectedEPlan.id + "&disciplineId=" + $scope.selectedTPlan.discipline.id;
    };

    $scope.saveEPlan = function(ePlan) {
        ePlanFactory.save(ePlan).success(function(data) {
            if (data.result != 0) {
                return;
            }
            ePlan.id = data.data;
        });
        $scope.$apply();
    };

    $scope.saveUnit = function(unit) {
        unitFactory.save(unit).success(function(data){
            if (data.result != 0) {
                return;
            }
            unit.id = data.data;
        });
        $scope.$apply();
    };

    $scope.refreshEPlans = function() {
        ePlanFactory.list().success(function(data){
            if (data.result != 0) {
                return;
            }
            $scope.ePlans = data.data;
        });
        $scope.selectedEPlan = {};
        $scope.prepareTPlanLink();
    };

    $scope.addEPlan = function() {
        var newPlan = {
            id: -1,
            speciality: $scope.newEPlan.speciality,
            yearBegin: $scope.newEPlan.yearBegin,
            yearEnd: $scope.newEPlan.yearEnd
        };
        if (typeof newPlan.speciality == 'undefined' ||
            typeof newPlan.yearBegin == 'undefined' ||
            typeof newPlan.yearEnd == 'undefined') {
            return;
        }
        if (newPlan.speciality.length < 6 ||
            newPlan.speciality.length > 10) {
            return;
        }
        if (newPlan.yearBegin < 1995 ||
            newPlan.yearBegin > newPlan.yearEnd ||
            newPlan.yearEnd > 2050) {
            return;
        }
        ePlanFactory.add(newPlan).success(function(data) {
            if (data.result != 0) {
                return;
            }
            newPlan.id = data.data;
            $scope.ePlans.push(newPlan);
        });
    };

    $scope.addTPlan = function(){
        if (isEmpty($scope.selectedEPlan)) {
            return;
        }
        if ($scope.newDiscipline.theory < 0 ||
            $scope.newDiscipline.practice < 0 ||
            $scope.newDiscipline.personal < 0) {
            return;
        }
        if (typeof $scope.newDiscipline.name == 'undefined' ||
            typeof $scope.newDiscipline.theory == 'undefined' ||
            typeof $scope.newDiscipline.practice == 'undefined' ||
            typeof $scope.newDiscipline.personal == 'undefined') {
            return;
        }
        var ePlan = $scope.selectedEPlan;
        var newDiscipline = {
            id: -1,
            name: $scope.newDiscipline.name,
            type: 'DISCIPLINE',
            theory: $scope.newDiscipline.theory,
            practice: $scope.newDiscipline.practice,
            control: $scope.newDiscipline.control,
            personal: $scope.newDiscipline.personal
        };
        var newTPlan = { discipline:newDiscipline, eplanId: ePlan.id, position:0 };
        tPlanFactory.add(newTPlan).success(function(data) {
            if (data.result != 0) {
                return;
            }
            newTPlan.discipline.id = data.data;
            newTPlan.disciplineId = data.data;
            ePlan.tPlans.push(newTPlan);
        });
    };

    $scope.deleteEPlan = function() {
        if (isEmpty($scope.selectedEPlan)) {
            return;
        }
        var ePlan = $scope.selectedEPlan;
        ePlanFactory.remove($scope.selectedEPlan.id).success(function(data) {
            if (data.result != 0) {
                return;
            }
            for(var i = 0; i < $scope.ePlans.length; i++) {
                if ($scope.ePlans[i].id == ePlan.id) {
                    $scope.ePlans.splice(i,1);
                    break;
                }
            }
            $scope.selectedEPlan = {};
            $scope.selectedTPlan = {};
            $scope.prepareTPlanLink();
        });
    };

    $scope.deleteTPlan = function() {
        if (isEmpty($scope.selectedTPlan) || isEmpty($scope.selectedEPlan)) {
            return;
        }
        var ePlan = $scope.selectedEPlan;
        var tPlan = $scope.selectedTPlan;
        tPlan.eplanId = ePlan.id;
        tPlanFactory.remove(tPlan).success(function (data) {
            if (data.result != 0) {
                return;
            }
            for (var i = 0; i < ePlan.tPlans.length; i++) {
                if (ePlan.tPlans[i].disciplineId == tPlan.disciplineId) {
                    ePlan.tPlans.splice(i,1);
                    break;
                }
            }
            $scope.selectedTPlan = {};
            $scope.prepareTPlanLink();
        });
    };

    $scope.refreshEPlans();
});