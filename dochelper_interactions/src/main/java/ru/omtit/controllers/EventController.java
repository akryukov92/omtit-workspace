package ru.omtit.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.omtit.model.CmdResult;
import ru.omtit.model.Event;
import ru.omtit.services.EventService;
import ru.omtit.services.ServiceException;

import java.util.Date;

@RestController
@RequestMapping("/event")
public class EventController {
    private static final Logger logger = Logger.getLogger(EventController.class);

    @Autowired
    private EventService eventService;

    @RequestMapping (
            method = RequestMethod.GET
    )
    @ResponseBody
    public CmdResult handleGet(@RequestParam(value = "groupId") int groupId,
                                             @RequestParam(value = "since") Date since,
                                             @RequestParam(value = "till") Date till) {
        logger.info("request /; arg: " + groupId);
        try {
            return new CmdResult(eventService.get(groupId, since, till));
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            return new CmdResult(e.getMessage(),1);
        }
    }

    @RequestMapping (
            value = "/item",
            method = RequestMethod.GET
    )
    @ResponseBody
    public CmdResult handleGetItem(@RequestParam(value = "unitId") int unitId, @RequestParam(value = "groupId") int groupId) {
        logger.info("request /item; unitId: " + unitId + " groupId: " + groupId);
        try {
            return new CmdResult(eventService.get(unitId, groupId));
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            return new CmdResult(e.getMessage(),1);
        }
    }

    @RequestMapping (
            value = "/add",
            method = RequestMethod.POST
    )
    @ResponseBody
    public CmdResult handleAdd(@RequestBody Event event) {
        logger.info("request /add; arg: " + event.getUnitId() + ";" + event.getGroupId());
        try {
            return new CmdResult(eventService.insert(event));
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            return new CmdResult(e.getMessage(), 1);
        }
    }

    @RequestMapping (
            value = "/delete",
            method = RequestMethod.POST
    )
    @ResponseBody
    public CmdResult handleDelete(@RequestBody Event event) {
        logger.info("request /delete; : " + event.getUnitId() + ";" + event.getGroupId());
        return new CmdResult(eventService.delete(event.getGroupId(),event.getUnitId()));
    }
}
