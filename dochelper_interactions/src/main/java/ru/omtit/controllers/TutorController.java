package ru.omtit.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.omtit.model.CmdResult;
import ru.omtit.model.Tutor;
import ru.omtit.services.ServiceException;
import ru.omtit.services.TutorService;

@RestController
@RequestMapping("/tutor")
public class TutorController {
    private static final Logger logger = Logger.getLogger(TutorController.class);

    @Autowired
    private TutorService service;

    @RequestMapping (
            method = RequestMethod.GET
    )
    @ResponseBody
    public CmdResult handleGet() {
        logger.info("request /");
        return new CmdResult(service.getList());
    }

    @RequestMapping (
            value = "/item",
            method = RequestMethod.GET
    )
    @ResponseBody
    public CmdResult handleGetItem(@RequestParam(value = "id") int tutorId) {
        logger.info("request /item; tutor id:" + tutorId);
        return new CmdResult(service.get(tutorId));
    }

    @RequestMapping (
            value = "/add",
            method = RequestMethod.POST
    )
    @ResponseBody
    public CmdResult handleAdd(@RequestBody final Tutor tutor) {
        logger.info("request /add; tutor data:" + tutor.toString());
        return new CmdResult(service.insert(tutor));
    }

    @RequestMapping (
            value = "/delete",
            method = RequestMethod.POST
    )
    @ResponseBody
    public CmdResult handleDelete(@RequestBody int tutorId) {
        logger.info("request /delete; tutor id: " + tutorId);
        try {
            return new CmdResult(service.delete(tutorId));
        } catch (ServiceException e) {
            return new CmdResult(e.getMessage(), 1);
        }
    }

    @RequestMapping (
            value = "/update",
            method = RequestMethod.POST
    )
    @ResponseBody
    public CmdResult handleUpdate(@RequestBody Tutor tutor) {
        logger.info("request /update; tutor data:" + tutor.toString());
        try {
            return new CmdResult(service.update(tutor));
        } catch (ServiceException e) {
            return new CmdResult(e.getMessage(), 1);
        }
    }
}
