package ru.omtit.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.omtit.model.CmdResult;
import ru.omtit.model.TPlan;
import ru.omtit.services.ServiceException;
import ru.omtit.services.TPlanService;

@RestController
@RequestMapping("/tplan")
public class TPlanController {

    private static final Logger logger = Logger.getLogger(TPlanController.class);

    @Autowired
    private TPlanService service;

    @RequestMapping (
            method = RequestMethod.GET
    )
    @ResponseBody
    public CmdResult handleGet(@RequestParam(value = "eplanId") int ePlanId) {
        logger.info("request /; arg: " + ePlanId);
        try {
            return new CmdResult(service.get(ePlanId));
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            return new CmdResult(e.getMessage(),1);
        }
    }

    @RequestMapping (
            value = "/item",
            method = RequestMethod.GET
    )
    @ResponseBody
    public CmdResult handleGetItem(@RequestParam(value = "eplanId") int ePlanId, @RequestParam(value = "disciplineId") int disciplineId) {
        logger.info("request /item; eplanId:" + ePlanId + " disciplineId: " + disciplineId);
        try {
            return new CmdResult(service.get(ePlanId, disciplineId));
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            return new CmdResult(e.getMessage(),1);
        }
    }

    @RequestMapping (
            value = "/add",
            method = RequestMethod.POST
    )
    @ResponseBody
    public CmdResult handleAdd(@RequestBody TPlan tPlan) {
        logger.info("request /add; arg:" + tPlan.getEPlanId() + " unit:" + tPlan.getDisciplineId());
        try {
            return new CmdResult(service.insert(tPlan));
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            return new CmdResult(e.getMessage(),1);
        }
    }

    @RequestMapping (
            value = "/delete",
            method = RequestMethod.POST
    )
    @ResponseBody
    public CmdResult handleDelete(@RequestBody TPlan tPlan) {
        logger.info("request /delete; arg:" + tPlan.getEPlanId() + " unit:" + tPlan.getDisciplineId());
        try {
            return new CmdResult(service.delete(tPlan.getEPlanId(), tPlan.getDisciplineId()));
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            return new CmdResult(e.getMessage(),1);
        }
    }
}
