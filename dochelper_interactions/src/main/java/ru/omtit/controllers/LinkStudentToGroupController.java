package ru.omtit.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.omtit.model.CmdResult;
import ru.omtit.model.LinkStudentToGroup;
import ru.omtit.services.LinkStudentToGroupService;
import ru.omtit.services.ServiceException;

@RestController
@RequestMapping("/studentlink")
public class LinkStudentToGroupController {

    private static Logger logger = Logger.getLogger(LinkStudentToGroupController.class);

    @Autowired
    private LinkStudentToGroupService linkService;

    @RequestMapping (
            method = RequestMethod.GET
    )
    @ResponseBody
    public CmdResult handleGet(@RequestParam(value = "studentId") int studentId) {
        logger.info("request /; arg:" + studentId);
        try {
            return new CmdResult(linkService.getByStudent(studentId));
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            return new CmdResult(e.getMessage(), 1);
        }
    }

    @RequestMapping (
            value = "/item",
            method = RequestMethod.GET
    )
    @ResponseBody
    public CmdResult handleGetItem(@RequestBody LinkStudentToGroup link) {
        logger.info("request /item: groupId: " + link.getGroupId() + " studentId:" + link.getStudentId());
        try {
            return new CmdResult(linkService.get(link.getGroupId(), link.getStudentId()));
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            return new CmdResult(e.getMessage(), 1);
        }
    }

    @RequestMapping (
            value = "/add",
            method = RequestMethod.POST
    )
    @ResponseBody
    public CmdResult handleAddItem(@RequestBody final LinkStudentToGroup link) {
        logger.info("request /add; link data: " + link.toString());
        try {
            return new CmdResult(linkService.insert(link));
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            return new CmdResult(e.getMessage(), 1);
        }
    }

    @RequestMapping (
            value = "/update",
            method = RequestMethod.POST
    )
    @ResponseBody
    public CmdResult handleUpdate(@RequestBody LinkStudentToGroup link) {
        logger.info("request /update: groupId: " + link.getGroupId() + " studentId:" + link.getStudentId());
        return new CmdResult("Обновление не поддерживается", 1);
    }

    @RequestMapping (
            value = "/delete",
            method = RequestMethod.POST
    )
    @ResponseBody
    public CmdResult handleDelete(@RequestBody LinkStudentToGroup link) {
        logger.info("request /delete: groupId: " + link.getGroupId() + " studentId:" + link.getStudentId());
        return new CmdResult(linkService.delete(link.getGroupId(), link.getStudentId()));
    }
}
