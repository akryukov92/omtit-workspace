package ru.omtit.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.omtit.model.CmdResult;
import ru.omtit.model.Unit;
import ru.omtit.services.ServiceException;
import ru.omtit.services.UnitService;

@RestController
@RequestMapping("/unit")
public class UnitController {

    private static final Logger logger = Logger.getLogger(UnitController.class);

    @Autowired
    private UnitService service;

    @RequestMapping (
            value = "/update",
            method = RequestMethod.POST
    )
    @ResponseBody
    public CmdResult handleUpdate(@RequestBody Unit unit) {
        logger.info("request /update: unit: " + unit.toString());
        try {
            return new CmdResult(service.update(unit));
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            return new CmdResult(e.getMessage(),1);
        }
    }

    @RequestMapping (
            value = "/add",
            method = RequestMethod.POST
    )
    @ResponseBody
    public CmdResult handleAdd(@RequestBody Unit unit) {
        logger.info("request /add; data:" + unit.toString());
        try {
            return new CmdResult(service.insert(unit));
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            return new CmdResult(e.getMessage(),1);
        }
    }

    @RequestMapping (
            value = "/delete",
            method = RequestMethod.POST
    )
    @ResponseBody
    public CmdResult handleDelete(@RequestBody int unitId) {
        logger.info("request /delete; unit id:" + unitId);
        return new CmdResult(service.delete(unitId));
    }
}
