package ru.omtit.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.omtit.model.CmdResult;
import ru.omtit.model.Student;
import ru.omtit.services.ServiceException;
import ru.omtit.services.StudentService;

@RestController
@RequestMapping("/student")
public class StudentController {

    private static final Logger logger = Logger.getLogger(StudentController.class);

    @Autowired
    private StudentService service;

    @RequestMapping (
            value = "/update",
            method = RequestMethod.POST
    )
    @ResponseBody
    public CmdResult handleUpdate(@RequestBody Student student) {
        logger.info("request /update: student: " + student.toString());
        try {
            return new CmdResult(service.update(student));
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            return new CmdResult(e.getMessage(),1);
        }
    }

    @RequestMapping (
            value = "/add",
            method = RequestMethod.POST
    )
    @ResponseBody
    public CmdResult handleAdd(@RequestBody Student student) {
        logger.info("request /add; data: " + student.toString());
        try {
            return new CmdResult(service.insert(student));
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            return new CmdResult(e.getMessage(),1);
        }
    }

    @RequestMapping (
            value = "/delete",
            method = RequestMethod.POST
    )
    @ResponseBody
    public CmdResult handleDelete(@RequestBody int studentId) {
        logger.info("request /delete; student id: " + studentId);
        return new CmdResult(service.delete(studentId));
    }
}
