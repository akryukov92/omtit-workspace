package ru.omtit.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.omtit.model.CmdResult;
import ru.omtit.model.LinkUnitToUnit;
import ru.omtit.services.LinkUnitToUnitService;
import ru.omtit.services.ServiceException;

@RestController
@RequestMapping("/unitlink")
public class LinkUnitToUnitController {

    private static final Logger logger = Logger.getLogger(LinkUnitToUnitController.class);

    @Autowired
    private LinkUnitToUnitService service;

    @RequestMapping (
            method = RequestMethod.GET
    )
    @ResponseBody
    public CmdResult handleGet(@RequestParam(value = "parentId") int parentId) {
        logger.info("request /; arg" + parentId);
        try {
            return new CmdResult(service.get(parentId));
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            return new CmdResult(e.getMessage(),1);
        }
    }

    @RequestMapping (
            value = "/item",
            method = RequestMethod.GET
    )
    @ResponseBody
    public CmdResult handleGetItem(@RequestParam(value = "parentId") int parentId, @RequestParam(value = "childId") int childId) {
        logger.info("request /item; parentId: " + parentId + " childId:" + childId);
        try {
            return new CmdResult(service.get(parentId,childId));
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            return new CmdResult(e.getMessage(),1);
        }
    }

    @RequestMapping (
            value = "/add",
            method = RequestMethod.POST
    )
    @ResponseBody
    public CmdResult handleAdd(@RequestBody LinkUnitToUnit link) {
        logger.info("request /add; parentid:" + link.getParentId() + " childId:" + link.getChildId());
        try {
            return new CmdResult(service.insert(link));
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            return new CmdResult(e.getMessage(),1);
        }
    }

    @RequestMapping (
            value = "/update",
            method = RequestMethod.POST
    )
    @ResponseBody
    public CmdResult handleUpdate(@RequestBody LinkUnitToUnit link) {
        logger.info("request /update: parentid:" + link.getParentId() + " childId:" + link.getChildId());
        try {
            return new CmdResult(service.update(link));
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            return new CmdResult(e.getMessage(), 1);
        }
    }

    @RequestMapping (
            value = "/delete",
            method = RequestMethod.POST
    )
    @ResponseBody
    public CmdResult handleDelete(@RequestBody LinkUnitToUnit link) {
        logger.info("request /delete; parentId: " + link.getParentId() + " childId:" + link.getChild().getId());

        return new CmdResult(service.delete(link.getParentId(), link.getChild().getId()));
    }
}
