package ru.omtit.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.omtit.model.CmdResult;
import ru.omtit.model.EPlan;
import ru.omtit.services.EPlanService;
import ru.omtit.services.ServiceException;

@RestController
@RequestMapping("/eplan")
public class EPlanController {

    private static final Logger logger  = Logger.getLogger(EPlanController.class);

    @Autowired
    private EPlanService service;

    @RequestMapping (
            method = RequestMethod.GET
    )
    @ResponseBody
    public CmdResult handleGet() {
        logger.info("request /");
        return new CmdResult(service.getList());
    }

    @RequestMapping (
            value = "/item",
            method = RequestMethod.GET
    )
    @ResponseBody
    public CmdResult handleGetItem(@RequestParam(value = "id") int ePlanId) {
        logger.info("request /item; plan id:" + ePlanId);
        return new CmdResult(service.get(ePlanId));
    }

    @RequestMapping (
            value = "/add",
            method = RequestMethod.POST
    )
    @ResponseBody
    public CmdResult handleAdd(@RequestBody final EPlan ePlan) {
        logger.info("request /add; plan data:" + ePlan.toString());
        return new CmdResult(service.insert(ePlan));
    }

    @RequestMapping (
            value = "/delete",
            method = RequestMethod.POST
    )
    @ResponseBody
    public CmdResult handleDelete(@RequestBody int ePlanId) {
        logger.info("request /delete; plan id: " + ePlanId);
        try {
            return new CmdResult(service.delete(ePlanId));
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            return new CmdResult(e.getMessage(), 1);
        }
    }

    @RequestMapping (
            value = "/update",
            method = RequestMethod.POST
    )
    @ResponseBody
    public CmdResult handleUpdate(@RequestBody EPlan ePlan) {
        logger.info("request /update; plan data: " + ePlan.toString());
        try {
            return new CmdResult(service.update(ePlan));
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            return new CmdResult(e.getMessage(), 1);
        }
    }
}
