package ru.omtit.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.omtit.model.CmdResult;
import ru.omtit.model.Group;
import ru.omtit.services.GroupService;
import ru.omtit.services.ServiceException;

@RestController
@RequestMapping("/group")
public class GroupController {

    private static final Logger logger = Logger.getLogger(GroupController.class);

    @Autowired
    private GroupService service;

    @RequestMapping (
            method = RequestMethod.GET
    )
    @ResponseBody
    public CmdResult handleGet() {
        logger.info("request /");
        return new CmdResult(service.getList());
    }

    @RequestMapping (
            value = "/list",
            method = RequestMethod.GET
    )
    @ResponseBody
    public CmdResult handleGetByEPlan(@RequestParam(value = "eplanId") int ePlanId) {
        logger.info("request /list; ePlanId: " + ePlanId);
        try {
            return new CmdResult(service.getList(ePlanId));
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            return new CmdResult(e.getMessage(),1);
        }
    }

    @RequestMapping (
            value = "/update",
            method = RequestMethod.POST
    )
    @ResponseBody
    public CmdResult handleUpdate(@RequestBody final Group group) {
        logger.info("request /update; group id: " + group.getId());
        try {
            return new CmdResult(service.update(group));
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            return new CmdResult(e.getMessage(),1);
        }
    }

    @RequestMapping (
            value = "/delete",
            method = RequestMethod.POST
    )
    @ResponseBody
    public CmdResult handleDelete(@RequestBody int groupId) {
        logger.info("request /delete; id: " + groupId);
        return new CmdResult(service.delete(groupId));
    }

    @RequestMapping (
            value = "/add",
            method = RequestMethod.POST
    )
    @ResponseBody
    public CmdResult handleAdd(@RequestBody Group group) {
        logger.info("request /add; id" + group.getId());
        try {
            return new CmdResult(service.insert(group));
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            return new CmdResult(e.getMessage(),1);
        }
    }
}
