package ru.omtit;

import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import ru.omtit.controllers.EPlanController;
import ru.omtit.model.Student;
import ru.omtit.services.*;
import ru.omtit.services.Impl.EPlanServiceImpl;

import static org.mockito.Mockito.*;

@Configuration
@EnableWebMvc
@EnableTransactionManagement
@ComponentScan("ru.omtit.controllers")
public class TestWebConfiguration extends WebMvcConfigurerAdapter {
    @Bean
    public EPlanService ePlanService() {
        return mock(EPlanService.class);
    }

    @Bean
    public EventService eventService() {return mock(EventService.class); }

    @Bean
    public GroupService groupService() { return mock(GroupService.class); }

    @Bean
    public LinkStudentToGroupService linkStudentToGroupService() { return mock(LinkStudentToGroupService.class); }

    @Bean
    public LinkUnitToUnitService linkUnitToUnitService() { return mock(LinkUnitToUnitService.class); }

    @Bean
    public StudentService studentService() { return mock(StudentService.class); }

    @Bean
    public TPlanService tPlanService() { return mock(TPlanService.class); }

    @Bean
    public UnitService unitService() { return mock(UnitService.class); }
}
