package ru.omtit.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.web.context.WebApplicationContext;
import ru.omtit.TestUtil;
import ru.omtit.TestWebAppInitializer;
import ru.omtit.TestWebConfiguration;
import ru.omtit.WebMvcConfig;
import ru.omtit.model.EPlan;
import ru.omtit.services.EPlanService;
import ru.omtit.services.ServiceException;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestWebAppInitializer.class, TestWebConfiguration.class, WebMvcConfig.class})
@WebAppConfiguration
public class EPlanControllerTest {
    private MockMvc mockMvc;

    private ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private EPlanService ePlanServiceMock;

    @Before
    public void setUp() {
        reset(ePlanServiceMock);
        mockMvc = webAppContextSetup(this.wac).build();
    }

    @Test
    public void callToIndexReturnsListOfPlans() throws Exception {
        EPlan ePlan1 = new EPlan(17),
                ePlan2 = new EPlan(19);
        List<EPlan> list = new ArrayList<>();
        list.add(ePlan1);
        list.add(ePlan2);

        when(ePlanServiceMock.getList()).thenReturn(list);
        mockMvc.perform(get("/eplan"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.*", hasSize(3)))
                .andExpect(jsonPath("$.data", hasSize(2)))
                .andExpect(jsonPath("$.message", is("")))
                .andExpect(jsonPath("$.result", is(0)));

        verify(ePlanServiceMock, times(1)).getList();
        verifyNoMoreInteractions(ePlanServiceMock);
    }

    @Test
    public void callToItemReturnsItem() throws Exception {
        EPlan ePlan = new EPlan(23);
        when(ePlanServiceMock.get(23)).thenReturn(ePlan);
        mockMvc.perform(get("/eplan/item?id=" + String.valueOf(23)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.*", hasSize(3)))
                .andExpect(jsonPath("$.message", is("")))
                .andExpect(jsonPath("$.result", is(0)))
                .andExpect(jsonPath("$.data.id", is(23)));
        verify(ePlanServiceMock, times(1)).get(23);
        verifyNoMoreInteractions(ePlanServiceMock);
    }

    @Test
    public void callAddReturnsNewId() throws Exception {
        EPlan ePlan = new EPlan(29);
        when(ePlanServiceMock.insert(ePlan)).thenReturn(29);
        MockHttpServletRequestBuilder requestBuilder = post("/eplan/add")
                .content(mapper.writeValueAsString(ePlan))
                .contentType(TestUtil.APPLICATION_JSON_UTF8);
        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.*", hasSize(3)))
                .andExpect(jsonPath("$.message", is("")))
                .andExpect(jsonPath("$.result", is(0)))
                .andExpect(jsonPath("$.data", is(29)));
        verify(ePlanServiceMock, times(1)).insert(ePlan);
        verifyNoMoreInteractions(ePlanServiceMock);
    }

    @Test
    public void callDeleteDefaultRun() throws Exception {
        when(ePlanServiceMock.delete(31)).thenReturn(1);
        MockHttpServletRequestBuilder requestBuilder = post("/eplan/delete")
                .content(mapper.writeValueAsString(31))
                .contentType(TestUtil.APPLICATION_JSON_UTF8);
        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.*", hasSize(3)))
                .andExpect(jsonPath("$.message", is("")))
                .andExpect(jsonPath("$.result", is(0)))
                .andExpect(jsonPath("$.data", is(1)));
        verify(ePlanServiceMock, times(1)).delete(31);
        verifyNoMoreInteractions(ePlanServiceMock);
    }

    @Test
    public void callDeleteFailRun() throws Exception {
        when(ePlanServiceMock.delete(37)).thenThrow(new ServiceException("Exception message"));
        MockHttpServletRequestBuilder requestBuilder = post("/eplan/delete")
                .content(mapper.writeValueAsString(37))
                .contentType(TestUtil.APPLICATION_JSON_UTF8);
        mockMvc.perform(requestBuilder)
                .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.*", hasSize(3)))
                .andExpect(jsonPath("$.message", is("Exception message")))
                .andExpect(jsonPath("$.result", is(1)));
        verify(ePlanServiceMock, times(1)).delete(37);
        verifyNoMoreInteractions(ePlanServiceMock);
    }
}
