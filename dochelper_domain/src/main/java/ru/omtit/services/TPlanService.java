package ru.omtit.services;

import ru.omtit.model.TPlan;

import java.util.List;

public interface TPlanService {
    int insert(TPlan tPlan) throws ServiceException;

    List<TPlan> get(int ePlanId) throws ServiceException;

    TPlan get(int ePlanId, int disciplineId) throws ServiceException;

    int delete(int ePlanId, int disciplineId) throws ServiceException;
}
