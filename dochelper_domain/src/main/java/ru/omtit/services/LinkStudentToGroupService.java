package ru.omtit.services;

import ru.omtit.model.LinkStudentToGroup;

import java.util.List;

public interface LinkStudentToGroupService {
    int insert(LinkStudentToGroup link) throws ServiceException;
    List<LinkStudentToGroup> getByGroup(int groupId) throws ServiceException;
    List<LinkStudentToGroup> getByStudent(int studentId) throws ServiceException;
    LinkStudentToGroup get(int groupId, int studentId) throws ServiceException;
    int delete(int groupId, int studentId);
    int deleteByStudent(int studentId);
}
