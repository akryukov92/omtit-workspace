package ru.omtit.services;

import ru.omtit.model.EPlan;

import java.util.List;

public interface EPlanService {
    int insert(EPlan ePlan);

    int update(EPlan ePlan) throws ServiceException;

    EPlan get(int ePlanId);

    List<EPlan> getList();

    int delete(int ePlanId) throws ServiceException;
}
