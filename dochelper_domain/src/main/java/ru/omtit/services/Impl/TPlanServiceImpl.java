package ru.omtit.services.Impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.omtit.dao.EPlanDAO;
import ru.omtit.dao.TPlanDAO;
import ru.omtit.dao.UnitDAO;
import ru.omtit.model.EPlan;
import ru.omtit.model.EPlanState;
import ru.omtit.model.TPlan;
import ru.omtit.model.Unit;
import ru.omtit.services.ServiceException;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional(timeout = 30)
public class TPlanServiceImpl implements ru.omtit.services.TPlanService {

    private static final Logger logger = Logger.getLogger(TPlanServiceImpl.class);

    @Autowired
    private EPlanDAO ePlanDAO;

    @Autowired
    private TPlanDAO tPlanDAO;

    @Autowired
    private UnitDAO unitDAO;

    @Override
    public int insert(TPlan tPlan) throws ServiceException {
        logger.info("add new tPlan");
        EPlan ePlan = ePlanDAO.get(tPlan.getEPlanId());
        if (ePlan == null) {
            throw new ServiceException("Не удалось добавить тематический план. Соответствующий учебный план не существует.");
        }
        if (ePlan.getState() == EPlanState.LOCKED) {
            throw new ServiceException("Учебный план защищен от редактирования.");
        }
        Unit discipline = unitDAO.get(tPlan.getDisciplineId());
        if (discipline == null) {
            logger.info("discipline for tPlan should be inserted");
            int newId = unitDAO.insert(tPlan.getDiscipline());
            tPlan.getDiscipline().setId(newId);
            tPlan.setDisciplineId(newId);
        } else {
            logger.info("discipline for tPlan should be updated");
            unitDAO.update(tPlan.getDiscipline());
        }
        tPlanDAO.insert(tPlan);
        return tPlan.getDiscipline().getId();
    }

    @Override
    public List<TPlan> get(int ePlanId) throws ServiceException {
        logger.info("get tPlans by ePlan");
        EPlan ePlan = ePlanDAO.get(ePlanId);
        if (ePlan == null) {
            throw new ServiceException("Не удалось извлечь тематические планы. Соответствующий учебный план не существует.");
        }
        List<TPlan> tPlans = tPlanDAO.get(ePlanId);
        List<TPlan> ret = new ArrayList<>();
        for (TPlan tPlan : tPlans) {
            Unit unit = unitDAO.get(tPlan.getDisciplineId());
            if (unit == null) {
                logger.warn("tPlan with reference to non-existent discipline was found and deleted.");
                tPlanDAO.delete(ePlanId, tPlan.getDisciplineId());
                continue;
            }
            tPlan.setDiscipline(unit);
            ret.add(tPlan);
        }
        return ret;
    }

    @Override
    public TPlan get(int ePlanId, int disciplineId) throws ServiceException {
        logger.info("get tPlan by composite id");
        EPlan ePlan = ePlanDAO.get(ePlanId);
        if (ePlan == null) {
            throw new ServiceException("Не удалось извлечь тематический план. Соответствующий учебный план не существует.");
        }
        Unit unit = unitDAO.get(disciplineId);
        if (unit == null) {
            throw new ServiceException("Не удалось извлечь тематический план. Указанной дисциплины не существует.");
        }
        TPlan tPlan = tPlanDAO.get(ePlanId, disciplineId);
        if (tPlan == null) {
            throw new ServiceException("Не удалось извлечь тематический план. В данном учебном плане не предусмотрено наличие данной дисциплины.");
        }
        tPlan.setDiscipline(unit);
        return tPlan;
    }

    @Override
    public int delete(int ePlanId, int disciplineId) throws ServiceException {
        logger.info("delete tPlan by composite id");
        EPlan ePlan = ePlanDAO.get(ePlanId);
        if (ePlan != null && ePlan.getState() == EPlanState.LOCKED) {
            throw new ServiceException("Учебный план защищен от редактирования.");
        }
        return tPlanDAO.delete(ePlanId, disciplineId);
    }
}