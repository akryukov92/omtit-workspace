package ru.omtit.services.Impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.omtit.dao.EPlanDAO;
import ru.omtit.dao.GroupDAO;
import ru.omtit.model.EPlan;
import ru.omtit.model.EPlanState;
import ru.omtit.model.Group;
import ru.omtit.services.GroupService;
import ru.omtit.services.ServiceException;

import java.util.List;

@Service
@Transactional(timeout = 30)
public class GroupServiceImpl implements GroupService{

    private static final Logger logger = Logger.getLogger(GroupServiceImpl.class);

    @Autowired
    private GroupDAO groupDAO;
    @Autowired
    private EPlanDAO ePlanDAO;

    @Override
    public int insert(Group group) throws ServiceException {
        logger.info("insert group");
        EPlan ePlan = ePlanDAO.get(group.getePlanId());
        if (ePlan == null) {
            throw new ServiceException("Не удалось добавить группу. Отсутствует соответствующий учебный план");
        }
        group.setEPlan(ePlan);
        int ret = groupDAO.insert(group);
        if (ePlan.getState() != EPlanState.LOCKED) {
            ePlan.setState(EPlanState.LOCKED);
            ePlanDAO.update(ePlan);
        }
        return ret;
    }

    @Override
    public int update(Group group) throws ServiceException {
        logger.info("update group");
        EPlan ePlan = ePlanDAO.get(group.getePlanId());
        if (ePlan == null) {
            throw new ServiceException("Не удалось изменить группу. Отсутствует соответствующий учебный план");
        }
        return groupDAO.update(group);
    }

    @Override
    public Group get(int groupId) {
        logger.info("get group by id");
        return groupDAO.getById(groupId);
    }

    @Override
    public List<Group> getList(int ePlanId) throws ServiceException {
        logger.info("get list of groups by ePlanId");
        EPlan ePlan = ePlanDAO.get(ePlanId);
        if (ePlan == null) {
            throw new ServiceException("Не удалось извлечь группы по учебному плану. Отсутствует учебный план.");
        }
        return groupDAO.getByEPlan(ePlanId);
    }

    @Override
    public List<Group> getList() {
        logger.info("get list of groups");
        return groupDAO.get();
    }

    @Override
    public int delete(int groupId) {
        logger.info("delete group by id");
        Group group = groupDAO.getById(groupId);
        int ret = 0;
        if (group != null) {
            int ePlanId = group.getePlanId();
            ret = groupDAO.delete(groupId);
            List<Group> groups = groupDAO.getByEPlan(ePlanId);
            if (groups.size() == 0) {
                EPlan ePlan = ePlanDAO.get(ePlanId);
                ePlan.setState(EPlanState.FREE);
                ePlanDAO.update(ePlan);
            }
        }
        return ret;
    }
}