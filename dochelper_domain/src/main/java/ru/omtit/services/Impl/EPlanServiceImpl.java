package ru.omtit.services.Impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.omtit.dao.EPlanDAO;
import ru.omtit.dao.TPlanDAO;
import ru.omtit.model.EPlan;
import ru.omtit.model.EPlanState;
import ru.omtit.services.EPlanService;
import ru.omtit.services.ServiceException;

import java.util.List;

@Service
@Transactional(timeout = 30)
public class EPlanServiceImpl implements EPlanService {

    @Autowired
    private EPlanDAO dao;
    @Autowired
    private TPlanDAO tPlanDAO;

    private static final Logger logger  = Logger.getLogger(EPlanServiceImpl.class);

    @Override
    public int insert(EPlan ePlan) {
        logger.info("insert ePlan");
        return dao.insert(ePlan);
    }

    @Override
    public int update(EPlan ePlan) throws ServiceException {
        logger.info("update ePlan");
        EPlan temp = dao.get(ePlan.getId());
        if (temp != null && temp.getState() == EPlanState.LOCKED) {
            throw new ServiceException("Учебный план защищен от редактирования.");
        }
        return dao.update(ePlan);
    }

    @Override
    public EPlan get(int ePlanId) {
        logger.info("get ePlan by id");
        return dao.get(ePlanId);
    }

    @Override
    public List<EPlan> getList() {
        logger.info("get ePlans");
        return dao.get();
    }

    @Override
    public int delete(int ePlanId) throws ServiceException {
        logger.info("delete ePlan");
        EPlan temp = dao.get(ePlanId);
        if (temp != null && temp.getState() == EPlanState.LOCKED) {
            throw new ServiceException("Учебный план защищен от редактирования.");
        }
        tPlanDAO.deleteByPlan(ePlanId);
        return dao.delete(ePlanId);
    }
}
