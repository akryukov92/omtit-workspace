package ru.omtit.services.Impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.omtit.dao.EventDAO;
import ru.omtit.dao.GroupDAO;
import ru.omtit.dao.TutorDAO;
import ru.omtit.dao.UnitDAO;
import ru.omtit.model.Event;
import ru.omtit.model.Group;
import ru.omtit.model.Tutor;
import ru.omtit.model.Unit;
import ru.omtit.services.EventService;
import ru.omtit.services.ServiceException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional(timeout = 30)
public class EventServiceImpl implements EventService {

    private static final Logger logger = Logger.getLogger(EventServiceImpl.class);

    @Autowired
    private EventDAO eventDAO;

    @Autowired
    private UnitDAO unitDAO;

    @Autowired
    private GroupDAO groupDAO;

    @Autowired
    private TutorDAO tutorDAO;

    @Override
    public int insert(Event event) throws ServiceException {
        logger.info("add new event");
        Unit unit = unitDAO.get(event.getUnitId());
        if (unit == null) {
            throw new ServiceException("Не удалось добавить событие. Соответствующее занятие не существует.");
        }
        Group group = groupDAO.getById(event.getGroupId());
        if (group == null) {
            throw new ServiceException("Не удалось добавить событие. Соответствующая группа не существует.");
        }
        Tutor tutor = tutorDAO.getById(event.getTutorId());
        if (tutor == null) {
            throw new ServiceException("Не удалось добавить событие. Соответствующий преподаватель не существует.");
        }
        eventDAO.insert(event);
        return event.getUnitId();
    }

    @Override
    public List<Event> get(int groupId, Date since, Date till) throws ServiceException {
        logger.info("get events by groupId");
        Group group = groupDAO.getById(groupId);
        if (group == null) {
            throw new ServiceException("Не удалось извлечь события. Запрошенной группы не существует.");
        }
        List<Event> eventList = eventDAO.get(groupId, since, till);
        List<Event> ret = new ArrayList<>(eventList.size());
        for (Event event: eventList) {
            Unit unit = unitDAO.get(event.getUnitId());
            if (unit == null) {
                logger.warn("event with reference to non-existent discipline was found and deleted");
                eventDAO.delete(groupId, event.getUnitId());
                continue;
            }
            event.setUnit(unit);
            ret.add(event);
        }
        return ret;
    }

    @Override
    public Event get(int groupId, int unitId) throws ServiceException {
        logger.info("get event by composite id");
        Unit unit = unitDAO.get(unitId);
        if (unit == null) {
            throw new ServiceException("Не удалось извлечь событие. Соответствующее занятие не существует.");
        }
        Group group = groupDAO.getById(groupId);
        if (group == null) {
            throw new ServiceException("Не удалось извлечь событие. Соответствующая группа не существует.");
        }
        Event event = eventDAO.get(groupId,unitId);
        if (event == null) {
            throw new ServiceException("Не удалось извлечь событие.");
        }
        return event;
    }

    @Override
    public int delete(int groupId, int unitId) {
        logger.info("delete event by composite key");
        return eventDAO.delete(groupId,unitId);
    }

    @Override
    public int update(Event event) throws ServiceException {
        logger.info("add new event");
        Unit unit = unitDAO.get(event.getUnitId());
        if (unit == null) {
            throw new ServiceException("Не удалось изменить событие. Соответствующее занятие не существует.");
        }
        Group group = groupDAO.getById(event.getGroupId());
        if (group == null) {
            throw new ServiceException("Не удалось изменить событие. Соответствующая группа не существует.");
        }
        Event temp = eventDAO.get(event.getGroupId(), event.getUnitId());
        if (temp == null) {
            throw new ServiceException("Не удалось изменить событие. Событие не существует.");
        }
        return eventDAO.update(event);
    }
}
