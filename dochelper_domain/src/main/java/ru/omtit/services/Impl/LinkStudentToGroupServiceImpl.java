package ru.omtit.services.Impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.omtit.dao.GroupDAO;
import ru.omtit.dao.LinkStudentToGroupDAO;
import ru.omtit.dao.StudentDAO;
import ru.omtit.model.Group;
import ru.omtit.model.LinkStudentToGroup;
import ru.omtit.model.Student;
import ru.omtit.services.LinkStudentToGroupService;
import ru.omtit.services.ServiceException;

import java.util.List;

@Service
@Transactional(timeout = 30)
public class LinkStudentToGroupServiceImpl implements LinkStudentToGroupService {

    private static final Logger logger = Logger.getLogger(LinkStudentToGroupServiceImpl.class);

    @Autowired
    private StudentDAO studentDAO;
    @Autowired
    private GroupDAO groupDAO;
    @Autowired
    private LinkStudentToGroupDAO linkDAO;

    public int insert(LinkStudentToGroup link) throws ServiceException {
        logger.info("insert LinkStudentToGroup");
        Group group = groupDAO.getById(link.getGroupId());
        if (group == null) {
            throw new ServiceException("Не удалось добавить связь группы со студентом. Группа не существует.");
        }
        Student student = studentDAO.get(link.getStudentId());
        if (student == null) {
            throw new ServiceException("Не удалось добавить связь группы со студентом. Студент не существует.");
        }
        linkDAO.insert(link);
        return link.getStudentId();
    }

    @Override
    public List<LinkStudentToGroup> getByGroup(int groupId) throws ServiceException {
        logger.info("get LinkStudentToGroup by group");
        Group group = groupDAO.getById(groupId);
        if(group == null) {
            throw new ServiceException("Не удалось извлечь связи группы со студентами");
        }
        return linkDAO.getByGroup(groupId);
    }

    @Override
    public List<LinkStudentToGroup> getByStudent(int studentId) throws ServiceException {
        logger.info("get LinkStudentToGroup by student");
        Student student = studentDAO.get(studentId);
        if (student == null) {
            throw new ServiceException("Не удалось извлечь связи студента с группами");
        }
        return linkDAO.getByStudent(studentId);
    }

    @Override
    public LinkStudentToGroup get(int groupId, int studentId) throws ServiceException {
        logger.info("get LinkStudentToGroup by composite key");
        LinkStudentToGroup link = linkDAO.get(groupId,studentId);
        if (link != null) {
            throw new ServiceException("Не удалось извлечь связь студента с группой");
        }
        return linkDAO.get(groupId,studentId);
    }

    @Override
    public int delete(int groupId, int studentId) {
        logger.info("delete LinkStudentToGroup by composite key");
        return linkDAO.delete(groupId,studentId);
    }

    @Override
    public int deleteByStudent(int studentId) {
        logger.info("delete LinkStudentToGroup by student id");
        return linkDAO.deleteByStudent(studentId);
    }
}
