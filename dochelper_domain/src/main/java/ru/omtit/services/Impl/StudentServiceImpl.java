package ru.omtit.services.Impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.omtit.dao.StudentDAO;
import ru.omtit.model.Student;
import ru.omtit.model.Utils;
import ru.omtit.services.ServiceException;
import ru.omtit.services.StudentService;

@Service
@Transactional(timeout = 30)
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentDAO studentDAO;

    private static final Logger logger = Logger.getLogger(StudentServiceImpl.class);

    public int insert(Student student) throws ServiceException {
        logger.info("insert student");
        if (student == null) {
            throw new ServiceException("Не удалось сохранить элемент. Отсутствуют данные для сохранения.");
        }
        logger.info("Inserting student:" + student.toString());
        if (student.getId() != Utils.DEFAULT_ENTITY_ID) {
            logger.warn("Inserting student with defined id");
        }
        int newId = studentDAO.insert(student);
        student.setId(newId);
        return student.getId();
    }

    @Override
    public int update(Student student) throws ServiceException {
        logger.info("update student");
        if (student == null) {
            throw new ServiceException("Не удалось сохранить элемент. Отсутствуют данные для сохранения.");
        }
        logger.info("Saving unit: " + student.toString());
        Student tempStudent = studentDAO.get(student.getId());
        if (tempStudent == null) {
            throw new ServiceException("Не удалось сохранить элемент. Элемент не существует.");
        }
        studentDAO.update(student);
        return student.getId();
    }

    @Override
    public int delete(int studentId) {
        logger.info("delete student");
        return studentDAO.delete(studentId);
    }

    @Override
    public Student get(int studentId) throws ServiceException {
        logger.info("get student by id");
        Student student = studentDAO.get(studentId);
        if (student == null) {
            throw new ServiceException("Не удалось извлечь элемент. Элемент не существует.");
        }
        return student;
    }
}
