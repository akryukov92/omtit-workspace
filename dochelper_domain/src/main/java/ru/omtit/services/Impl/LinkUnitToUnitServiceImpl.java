package ru.omtit.services.Impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.omtit.dao.LinkUnitToUnitDAO;
import ru.omtit.dao.UnitDAO;
import ru.omtit.model.LinkUnitToUnit;
import ru.omtit.model.Unit;
import ru.omtit.services.LinkUnitToUnitService;
import ru.omtit.services.ServiceException;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional(timeout = 30)
public class LinkUnitToUnitServiceImpl implements LinkUnitToUnitService {

    private static final Logger logger = Logger.getLogger(LinkUnitToUnitServiceImpl.class);

    @Autowired
    private UnitDAO unitDAO;
    @Autowired
    private LinkUnitToUnitDAO linkDAO;

    @Override
    public int insert(LinkUnitToUnit link) throws ServiceException {
        logger.info("insert LinkUnitToUnit");
        if (link.getChild() == null) {
            throw new ServiceException("Не удалось добавить связь с элементом. Данные о дочернем элементе отсутствуют");
        }
        Unit parent = unitDAO.get(link.getParentId());
        if (parent == null) {
            throw new ServiceException("Не удалось добавить связь с элементом. Родительский элемент не существует.");
        }
        if (!parent.isGreaterThan(link.getChild())) {
            throw new ServiceException("Не удалось добавить связь с элементом. Невозможная комбинация типов дочернего и родительского элементов.");
        }

        Unit unit = unitDAO.get(link.getChildId());
        if (unit == null) {
            logger.info("unit for this link should be inserted");
            int newId = unitDAO.insert(link.getChild());
            link.getChild().setId(newId);
            link.setChildId(newId);
        } else {
            logger.info("unit for this link should be updated");
            unitDAO.update(link.getChild());
        }
        linkDAO.insert(link);
        return link.getChild().getId();
    }

    @Override
    public List<LinkUnitToUnit> get(int parentId) throws ServiceException {
        logger.info("get list of LinkUnitToUnit by parent");
        Unit parent = unitDAO.get(parentId);
        if (parent == null) {
            throw new ServiceException("Не удалось получить связи с элементом. Родительский элемент не существует");
        }

        List<LinkUnitToUnit> links = linkDAO.get(parentId);
        List<LinkUnitToUnit> ret = new ArrayList<>();
        for(LinkUnitToUnit link : links) {
            Unit unit = unitDAO.get(link.getChildId());
            if (unit == null) {
                logger.warn("linkUnitToUnit references to non-existent discipline");
                linkDAO.delete(parentId, link.getChildId());
                continue;
            }
            link.setChild(unit);
            ret.add(link);
        }
        return ret;
    }

    @Override
    public LinkUnitToUnit get(int parentId, int childId) throws ServiceException {
        logger.info("get LinkUnitToUnit by composite key");
        Unit parent = unitDAO.get(parentId);
        if (parent == null) {
            throw new ServiceException("Не удалось получить связь между элементами. Родительский элемент не существует");
        }
        Unit child = unitDAO.get(childId);
        if (child == null) {
            throw new ServiceException("Не удалось получить связь между элементами. Дочерний элемент не сущесвует");
        }

        LinkUnitToUnit link = linkDAO.get(parentId, childId);
        if (link == null) {
            throw new ServiceException("Не удалось получить связь между элементами. Связь отсутствует.");
        }
        link.setChild(child);
        return link;
    }

    @Override
    public int update(LinkUnitToUnit link) throws ServiceException {
        logger.info("update LinkUnitToUnit");
        Unit parent = unitDAO.get(link.getParentId());
        if (parent == null) {
            throw new ServiceException("Не удалось обновить связь между элементами. Родительский элемент не существует");
        }
        Unit child = unitDAO.get(link.getChildId());
        if (child == null) {
            throw new ServiceException("Не удалось обновить связь между элементами. Дочерний элемент не существует");
        }
        LinkUnitToUnit temp = linkDAO.get(link.getParentId(), link.getChildId());
        if (temp == null) {
            return linkDAO.insert(link);
        }
        return linkDAO.update(link);
    }

    @Override
    public int delete(int parentId, int childId) {
        logger.info("delete LinkUnitToUnit by composite key");
        return linkDAO.delete(parentId, childId);
    }
}
