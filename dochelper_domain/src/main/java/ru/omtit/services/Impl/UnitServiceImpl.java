package ru.omtit.services.Impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.omtit.dao.LinkUnitToUnitDAO;
import ru.omtit.dao.UnitDAO;
import ru.omtit.model.Unit;
import ru.omtit.model.Utils;
import ru.omtit.services.ServiceException;

import java.util.List;

@Service
@Transactional(timeout = 30)
public class UnitServiceImpl implements ru.omtit.services.UnitService {

    @Autowired
    private UnitDAO unitDAO;

    @Autowired
    private LinkUnitToUnitDAO linkDAO;

    private static final Logger logger  = Logger.getLogger(UnitServiceImpl.class);

    @Override
    public List<Unit> getUnusedDisciplines() {
        return unitDAO.getUnusedDisciplines();
    }

    @Override
    public int insert(Unit unit) throws ServiceException {
        if (unit == null) {
            throw new ServiceException("Не удалось сохранить элемент. Отсутствуют данные для сохранения.");
        }
        logger.info("Inserting unit:" + unit.toString());
        if (unit.getId() != Utils.DEFAULT_ENTITY_ID) {
            logger.warn("Inserting unit with defined id");
        }
        int newId = unitDAO.insert(unit);
        unit.setId(newId);
        return unit.getId();
    }

    @Override
    public int update(Unit unit) throws ServiceException {
        if (unit == null) {
            throw new ServiceException("Не удалось сохранить элемент. Отсутствуют данные для сохранения.");
        }
        logger.info("Saving unit: " + unit.toString());
        Unit tempUnit = unitDAO.get(unit.getId());
        if (tempUnit == null){
            throw new ServiceException("Не удалось сохранить элемент. Элемент не существует.");
        }
        unitDAO.update(unit);
        return unit.getId();
    }

    @Override
    public int delete(int unitId) {
        logger.info("Deleting unit with id: " + unitId);
        unitDAO.delete(unitId);
        logger.info("Deleting links from children to unit with id: " + unitId);
        return linkDAO.deleteByParent(unitId);
    }

    @Override
    public Unit get(int unitId) throws ServiceException {
        Unit unit = unitDAO.get(unitId);
        if (unit == null) {
            throw new ServiceException("Не удалось извлечь элемент. Элемент не существует.");
        }
        return unit;
    }
}
