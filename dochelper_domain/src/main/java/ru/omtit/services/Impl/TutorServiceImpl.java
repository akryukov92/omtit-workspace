package ru.omtit.services.Impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.omtit.dao.TutorDAO;
import ru.omtit.model.Tutor;
import ru.omtit.services.ServiceException;
import ru.omtit.services.TutorService;

import java.util.List;

@Service
@Transactional(timeout = 30)
public class TutorServiceImpl implements TutorService {

    private static final Logger logger = Logger.getLogger(TutorServiceImpl.class);

    @Autowired
    private TutorDAO tutorDAO;

    public int insert(Tutor tutor) {
        logger.info("insert tutor");
        return tutorDAO.insert(tutor);
    }

    @Override
    public int update(Tutor tutor) throws ServiceException {
        logger.info("update tutor");
        Tutor temp = tutorDAO.getById(tutor.getId());
        if (temp == null) {
            throw new ServiceException("Не удалось сохранить элемент. Элемент не существует.");
        }
        return tutorDAO.update(tutor);
    }

    @Override
    public Tutor get(int tutorId) {
        logger.info("get tutor by id");
        return tutorDAO.getById(tutorId);
    }

    @Override
    public List<Tutor> getList() {
        logger.info("get tutors");
        return tutorDAO.get();
    }

    @Override
    public int delete(int tutorId) throws ServiceException {
        logger.info("delete tutor");
        return tutorDAO.delete(tutorId);
    }
}
