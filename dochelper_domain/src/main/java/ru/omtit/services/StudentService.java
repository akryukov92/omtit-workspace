package ru.omtit.services;

import ru.omtit.model.Student;

public interface StudentService {
    int insert(Student student) throws ServiceException;

    int update(Student student) throws ServiceException;

    int delete(int studentId);

    Student get(int studentId) throws ServiceException;
}
