package ru.omtit.services;

import ru.omtit.model.Group;

import java.util.List;

public interface GroupService {
    int insert(Group group) throws ServiceException;
    int update(Group group) throws ServiceException;
    Group get(int groupId);
    List<Group> getList(int ePlanId) throws ServiceException;
    List<Group> getList();
    int delete(int groupId);
}
