package ru.omtit.services;

import ru.omtit.model.Tutor;

import java.util.List;

public interface TutorService {
    int insert(Tutor tutor);
    int update(Tutor tutor) throws ServiceException;
    Tutor get(int tutorId);
    List<Tutor> getList();
    int delete(int tutorId) throws ServiceException;
}
