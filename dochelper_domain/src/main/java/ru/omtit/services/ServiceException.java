package ru.omtit.services;

public class ServiceException extends Exception {
    public ServiceException(String message) {
        super(message);
    }
}
