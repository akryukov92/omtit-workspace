package ru.omtit.services;

import ru.omtit.model.Event;

import java.util.Date;
import java.util.List;

public interface EventService {
    int insert(Event event) throws ServiceException;
    List<Event> get(int groupId, Date since, Date till) throws ServiceException;
    Event get(int groupId, int unitId) throws ServiceException;
    int delete(int groupId, int unitId);
    int update(Event event) throws ServiceException;
}
