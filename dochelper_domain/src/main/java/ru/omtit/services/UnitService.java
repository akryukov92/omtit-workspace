package ru.omtit.services;

import ru.omtit.model.Unit;

import java.util.List;

public interface UnitService {
    List<Unit> getUnusedDisciplines();

    int insert(Unit unit) throws ServiceException;

    int update(Unit unit) throws ServiceException;

    int delete(int unitId);

    Unit get(int unitId) throws ServiceException;
}
