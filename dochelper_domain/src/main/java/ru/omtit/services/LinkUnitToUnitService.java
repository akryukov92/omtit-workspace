package ru.omtit.services;

import ru.omtit.model.LinkUnitToUnit;

import java.util.List;

public interface LinkUnitToUnitService {
    int insert(LinkUnitToUnit link) throws ServiceException;

    List<LinkUnitToUnit> get(int parentId) throws ServiceException;

    LinkUnitToUnit get(int parentId, int childId) throws ServiceException;

    int update(LinkUnitToUnit link) throws ServiceException;

    int delete(int parentId, int childId);
}
