package ru.omtit.dao;

import ru.omtit.model.Student;

public interface StudentDAO {
    Student get(int id);
    int delete(int studentId);
    int insert(Student student);
    void update(Student student);
}
