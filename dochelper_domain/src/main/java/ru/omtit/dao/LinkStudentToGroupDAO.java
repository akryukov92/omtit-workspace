package ru.omtit.dao;

import ru.omtit.model.LinkStudentToGroup;

import java.util.List;

public interface LinkStudentToGroupDAO {
    List<LinkStudentToGroup> getByGroup(int groupId);
    List<LinkStudentToGroup> getByStudent(int studentId);
    LinkStudentToGroup get(int groupId, int studentId);
    int delete(int groupId, int studentId);
    int deleteByStudent(int studentId);
    void insert(LinkStudentToGroup linkStudentToGroup);
}
