package ru.omtit.dao;

import ru.omtit.model.LinkUnitToUnit;

import java.util.List;

public interface LinkUnitToUnitDAO {
    List<LinkUnitToUnit> get(int parentId);
    LinkUnitToUnit get(int parentId, int childId);
    int deleteByParent(int parentId);
    int delete(int parentId, int childId);
    int insert(LinkUnitToUnit linkUnitToUnit);
    int update(LinkUnitToUnit linkUnitToUnit);
}
