package ru.omtit.dao.Impl;

import org.springframework.stereotype.Repository;
import ru.omtit.dao.TutorDAO;
import ru.omtit.model.Tutor;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class TutorDAOImpl implements TutorDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Tutor> get() {
        return em.createQuery("from Tutor", Tutor.class)
                .getResultList();
    }

    @Override
    public Tutor getById(int id) {
        return em.find(Tutor.class, id);
    }

    @Override
    public int delete(int tutorId) {
        return em.createQuery("delete from Tutor where id = :tutorId")
                .setParameter("tutorId", tutorId)
                .executeUpdate();
    }

    @Override
    public int insert(Tutor tutor) {
        em.persist(tutor);
        return tutor.getId();
    }

    @Override
    public int update(Tutor tutor) {
        em.merge(tutor);
        return tutor.getId();
    }
}
