package ru.omtit.dao.Impl;

import org.springframework.stereotype.Repository;
import ru.omtit.dao.UnitDAO;
import ru.omtit.model.Unit;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

@Repository
public class UnitDAOImpl implements UnitDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Unit get(final int id) {
        return em.find(Unit.class, id);
    }

    @Override
    public List<Unit> getUnusedDisciplines() {
        return new ArrayList<>();
    }

    @Override
    public int delete(int unitId) {
        return em.createQuery("delete from Unit where id = :id")
                .setParameter("id", unitId)
                .executeUpdate();
    }

    @Override
    public int insert(Unit unit) {
        em.persist(unit);
        return unit.getId();
    }

    @Override
    public void update(Unit unit) {
        em.merge(unit);
    }
}
