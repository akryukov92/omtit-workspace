package ru.omtit.dao.Impl;

import org.springframework.stereotype.Repository;
import ru.omtit.dao.TPlanDAO;
import ru.omtit.model.TPlan;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class TPlanDAOImpl implements TPlanDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<TPlan> get(final int ePlanId) {
        return em.createQuery("from TPlan where key.ePlanId = :ePlanId", TPlan.class)
                .setParameter("ePlanId", ePlanId)
                .getResultList();
    }

    @Override
    public TPlan get(final int ePlanId, final int disciplineId) {
        TPlan.TPlanId primaryKey = new TPlan.TPlanId();
        primaryKey.setDisciplineId(disciplineId);
        primaryKey.setEPlanId(ePlanId);
        return em.find(TPlan.class, primaryKey);
    }

    @Override
    public int delete(int ePlanId, int disciplineId) {
        return em.createQuery("delete from TPlan where key.ePlanId = :ePlanId and key.disciplineId = :disciplineId")
                .setParameter("ePlanId", ePlanId)
                .setParameter("disciplineId", disciplineId)
                .executeUpdate();
    }

    @Override
    public int deleteByPlan(int ePlanId) {
        return em.createQuery("delete from TPlan where key.ePlanId = :ePlanId")
                .setParameter("ePlanId", ePlanId)
                .executeUpdate();
    }

    @Override
    public void update(TPlan tPlan) {
        em.merge(tPlan);
    }

    @Override
    public void insert(TPlan tPlan) {
        em.persist(tPlan);
    }
}
