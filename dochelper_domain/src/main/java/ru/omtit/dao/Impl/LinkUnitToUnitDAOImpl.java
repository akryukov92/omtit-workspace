package ru.omtit.dao.Impl;

import org.springframework.stereotype.Repository;
import ru.omtit.dao.LinkUnitToUnitDAO;
import ru.omtit.model.LinkUnitToUnit;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class LinkUnitToUnitDAOImpl implements LinkUnitToUnitDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<LinkUnitToUnit> get(final int parentId) {
        return em.createQuery("from LinkUnitToUnit where key.parentId = :parentId", LinkUnitToUnit.class)
                .setParameter("parentId", parentId)
                .getResultList();
    }

    @Override
    public LinkUnitToUnit get(final int parentId, final int childId) {
        LinkUnitToUnit.LinkUnitToUnitId primaryKey = new LinkUnitToUnit.LinkUnitToUnitId();
        primaryKey.setParentId(parentId);
        primaryKey.setChildId(childId);
        return em.find(LinkUnitToUnit.class, primaryKey);
    }

    @Override
    public int deleteByParent(int parentId) {
        return em.createQuery("delete from LinkUnitToUnit where key.parentId = :parentId")
                .setParameter("parentId", parentId)
                .executeUpdate();
    }

    @Override
    public int delete(int parentId, int childId) {
        return em.createQuery("delete from LinkUnitToUnit where key.parentId = :parentId and key.childId = :childId")
                .setParameter("childId", childId)
                .setParameter("parentId", parentId)
                .executeUpdate();
    }

    @Override
    public int insert(LinkUnitToUnit linkUnitToUnit) {
        em.persist(linkUnitToUnit);
        return linkUnitToUnit.getChildId();
    }

    @Override
    public int update(LinkUnitToUnit linkUnitToUnit) {
        em.merge(linkUnitToUnit);
        return linkUnitToUnit.getChildId();
    }
}
