package ru.omtit.dao.Impl;

import org.springframework.stereotype.Repository;
import ru.omtit.dao.GroupDAO;
import ru.omtit.model.Group;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class GroupDAOImpl implements GroupDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Group> getByEPlan(int ePlanId) {
        return em.createQuery("from Group where ePlanId = :ePlanId", Group.class)
                .setParameter("ePlanId", ePlanId)
                .getResultList();
    }

    @Override
    public List<Group> get() {
        return em.createQuery("from Group", Group.class)
                .getResultList();
    }

    @Override
    public Group getById(int id) {
        return em.find(Group.class, id);
    }

    @Override
    public int delete(int groupId) {
        return em.createQuery("delete from Group where id = :groupId")
                .setParameter("groupId", groupId)
                .executeUpdate();
    }

    @Override
    public int insert(Group group) {
        em.persist(group);
        return group.getId();
    }

    @Override
    public int update(Group group) {
        em.merge(group);
        return group.getId();
    }
}
