package ru.omtit.dao.Impl;

import org.springframework.stereotype.Repository;
import ru.omtit.dao.StudentDAO;
import ru.omtit.model.Student;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class StudentDAOImpl implements StudentDAO {
    @PersistenceContext
    private EntityManager em;

    @Override
    public Student get(final int id) {
        return em.find(Student.class, id);
    }

    @Override
    public int delete(int studentId) {
        return em.createQuery("delete from Student where id = :id")
                .setParameter("id", studentId)
                .executeUpdate();
    }

    @Override
    public int insert(Student student) {
        em.persist(student);
        return student.getId();
    }

    @Override
    public void update(Student student) {
        em.merge(student);
    }
}
