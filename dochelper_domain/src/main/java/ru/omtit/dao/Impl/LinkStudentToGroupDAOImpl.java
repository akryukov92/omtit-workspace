package ru.omtit.dao.Impl;

import org.springframework.stereotype.Repository;
import ru.omtit.dao.LinkStudentToGroupDAO;
import ru.omtit.model.LinkStudentToGroup;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class LinkStudentToGroupDAOImpl implements LinkStudentToGroupDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<LinkStudentToGroup> getByGroup(int groupId) {
        return em.createQuery("from LinkStudentToGroup where key.groupId = :groupId", LinkStudentToGroup.class)
                .setParameter("groupId", groupId)
                .getResultList();
    }

    @Override
    public List<LinkStudentToGroup> getByStudent(int studentId) {
        return em.createQuery("from LinkStudentToGroup where key.studentId = :studentId", LinkStudentToGroup.class)
                .setParameter("studentId", studentId)
                .getResultList();
    }

    @Override
    public LinkStudentToGroup get(int groupId, int studentId) {
        LinkStudentToGroup.LinkStudentToGroupId primaryKey = new LinkStudentToGroup.LinkStudentToGroupId();
        primaryKey.setStudentId(studentId);
        primaryKey.setGroupId(groupId);
        return em.find(LinkStudentToGroup.class, primaryKey);
    }

    @Override
    public int delete(int groupId, int studentId) {
        return em.createQuery("delete from LinkStudentToGroup  where key.groupId = :groupId and key.studentId = :studentId")
                .setParameter("groupId", groupId)
                .setParameter("studentId", studentId)
                .executeUpdate();
    }

    @Override
    public int deleteByStudent(int studentId) {
        return em.createQuery("delete from LinkStudentToGroup where key.studentId = :studentId")
                .setParameter("studentId", studentId)
                .executeUpdate();
    }

    @Override
    public void insert(LinkStudentToGroup linkStudentToGroup) {
        em.persist(linkStudentToGroup);
    }
}
