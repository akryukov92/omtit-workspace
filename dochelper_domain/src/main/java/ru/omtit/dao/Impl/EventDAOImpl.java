package ru.omtit.dao.Impl;

import org.springframework.stereotype.Repository;
import ru.omtit.dao.EventDAO;
import ru.omtit.model.Event;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;
import java.util.List;

@Repository
public class EventDAOImpl implements EventDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Event> get(int groupId, Date since, Date till) {
        return em.createQuery("from Event where key.groupId = :groupId", Event.class)
                .setParameter("groupId", groupId)
                .getResultList();
    }

    @Override
    public Event get(int groupId, int unitId) {
        Event.EventId primaryKey = new Event.EventId();
        primaryKey.setGroupId(groupId);
        primaryKey.setUnitId(unitId);
        return em.find(Event.class, primaryKey);
    }

    @Override
    public int delete(int groupId, int unitId) {
        return em.createQuery("delete from Event where key.groupId = :groupId and key.unitId = :unitId")
                .setParameter("groupId", groupId)
                .setParameter("unitId", unitId)
                .executeUpdate();
    }

    @Override
    public int update(Event event) {
        em.merge(event);
        return event.getUnitId();
    }

    @Override
    public void insert(Event event) {
        em.persist(event);
    }
}
