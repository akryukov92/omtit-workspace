package ru.omtit.dao;

import ru.omtit.model.Tutor;

import java.util.List;

public interface TutorDAO {
    List<Tutor> get();
    Tutor getById(int id);
    int delete(int tutorId);
    int insert(Tutor tutor);
    int update(Tutor tutor);
}
