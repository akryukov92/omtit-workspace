package ru.omtit.dao.Impl;

import org.springframework.stereotype.Repository;
import ru.omtit.dao.EPlanDAO;
import ru.omtit.model.EPlan;
import ru.omtit.model.EPlanState;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class EPlanDAOImpl implements EPlanDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<EPlan> get() {
        return em.createQuery("from EPlan", EPlan.class).getResultList();
    }

    @Override
    public EPlan get(int id) {
        return em.find(EPlan.class, id);
    }

    @Override
    public int delete(int ePlanId) {
        return em.createQuery("delete from EPlan where id = :id and state != 'LOCKED'")
                .setParameter("id", ePlanId)
                .executeUpdate();
    }

    @Override
    public int insert(EPlan ePlan) {
        em.persist(ePlan);
        return ePlan.getId();
    }

    @Override
    public int update(EPlan ePlan) {
        EPlan temp = em.find(EPlan.class, ePlan.getId());
        if (temp.getState() == EPlanState.FREE) {
            em.merge(ePlan);
        }
        return ePlan.getId();
    }
}
