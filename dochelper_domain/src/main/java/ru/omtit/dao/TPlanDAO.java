package ru.omtit.dao;

import ru.omtit.model.TPlan;

import java.util.List;

public interface TPlanDAO {
    List<TPlan> get(int ePlanId);
    TPlan get(int ePlanId, int disciplineId);
    int delete(int ePlanId, int disciplineId);
    int deleteByPlan(int ePlanId);
    void update(TPlan tPlan);
    void insert(TPlan tPlan);
}

