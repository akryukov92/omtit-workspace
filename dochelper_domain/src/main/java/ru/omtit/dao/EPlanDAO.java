package ru.omtit.dao;

import ru.omtit.model.EPlan;

import java.util.List;

public interface EPlanDAO {
    List<EPlan> get();
    EPlan get(int id);
    int delete(int ePlanId);
    int insert(EPlan ePlan);
    int update(EPlan ePlan);
}
