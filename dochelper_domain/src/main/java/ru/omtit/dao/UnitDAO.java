package ru.omtit.dao;

import ru.omtit.model.Unit;

import java.util.List;

public interface UnitDAO {
    Unit get(int id);
    List<Unit> getUnusedDisciplines();
    int delete(int unitId);
    int insert(Unit unit);
    void update(Unit unit);
}
