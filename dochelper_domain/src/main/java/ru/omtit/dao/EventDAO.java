package ru.omtit.dao;

import ru.omtit.model.Event;

import java.util.Date;
import java.util.List;

public interface EventDAO {
    List<Event> get(int groupId, Date since, Date till);
    Event get(int groupId, int unitId);
    int delete(int groupId, int unitId);
    int update(Event event);
    void insert(Event event);
}
