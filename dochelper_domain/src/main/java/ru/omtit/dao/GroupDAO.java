package ru.omtit.dao;

import ru.omtit.model.Group;

import java.util.List;

public interface GroupDAO {
    List<Group> getByEPlan(int ePlanId);
    List<Group> get();
    Group getById(int id);
    int delete(int groupId);
    int insert(Group group);
    int update(Group group);
}
