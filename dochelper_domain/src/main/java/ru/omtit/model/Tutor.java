package ru.omtit.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "tutors")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Tutor {
    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tutor_id_seq")
    @SequenceGenerator(name = "tutor_id_seq", sequenceName = "tutor_id_seq")
    @JsonProperty("id")
    private int id;

    @Column (name = "name")
    @JsonProperty("name")
    @Size(min = 10, max = 200)
    private String name = "";

    public Tutor() {
        this(Utils.DEFAULT_ENTITY_ID, "");
    }

    public Tutor(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
