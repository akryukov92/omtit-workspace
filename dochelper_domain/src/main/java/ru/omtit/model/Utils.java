package ru.omtit.model;

import com.google.common.base.Strings;

/**
 * Created by AKryukov on 17.07.2015.
 */
public class Utils {
    public static final int DEFAULT_ENTITY_ID = -1;

    public static int tryParse(String string){
        return tryParse(string, 0);
    }

    public static int tryParse(String string, int defaultValue) {
        if (Strings.isNullOrEmpty(string)){
            return defaultValue;
        }
        try {
            return Integer.parseInt(string);
        } catch(NumberFormatException ex){
            return defaultValue;
        }
    }
}