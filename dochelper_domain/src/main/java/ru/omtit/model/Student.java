package ru.omtit.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;

@Entity
@Table(name = "students")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Student {
    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "student_id_seq")
    @SequenceGenerator(name = "student_id_seq", sequenceName = "student_id_seq")
    @JsonProperty(value = "id")
    private int id;

    @Column(name = "name")
    @JsonProperty(value = "name")
    @Size(min = 10, max = 200)
    private String name = "";

    @Column(name = "birth_date")
    @JsonProperty(value = "birthDate")
    @Type(type = "date")
    private Date birthDate;

    @Column(name = "address")
    @JsonProperty(value = "address")
    @Size(min = 10, max = 200)
    private String address;

    @Column(name = "education")
    @JsonProperty(value = "education")
    @Size(max = 200)
    private String education;

    @Column(name = "source_org")
    @JsonProperty(value = "source_org")
    private String sourceOrganization;

    public Student() {
        this(Utils.DEFAULT_ENTITY_ID, "", new Date(Calendar.getInstance().getTimeInMillis()), "", "", "");
    }

    public Student(int id, String name, Date birthDate, String address, String education, String sourceOrganization) {
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.address = address;
        this.education = education;
        this.sourceOrganization = sourceOrganization;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getSourceOrganization() {
        return sourceOrganization;
    }

    public void setSourceOrganization(String sourceOrganization) {
        this.sourceOrganization = sourceOrganization;
    }
}
