package ru.omtit.model;

/**
 * Created by AKryukov on 17.07.2015.
 */
public enum UnitType {
    UNDEFINED,
    DISCIPLINE,
    PART,
    THEME,
    ACTIVITY
}
