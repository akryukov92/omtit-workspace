package ru.omtit.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Objects;

@Entity
@Table(name = "e_plans")
@JsonIgnoreProperties(ignoreUnknown = true)
public class EPlan {

    @Column(name="id")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "e_plan_id_seq")
    @SequenceGenerator(name = "e_plan_id_seq", sequenceName = "e_plan_id_seq")
    @JsonProperty("id")
    private int id;

    @Column(name = "year_begin")
    @Range(min = 1995, max = 2050)
    @JsonProperty("yearBegin")
    private int yearBegin;

    @Column(name = "year_end")
    @Range(min = 1995, max = 2050)
    @JsonProperty("yearEnd")
    private int yearEnd;

    @Column(name = "speciality_code")
    @NotNull
    @Size(min = 6, max = 10)
    @JsonProperty("speciality")
    private String speciality;

    @Column(name = "state")
    @Enumerated(EnumType.STRING)
    @JsonProperty("state")
    private EPlanState state;

    public EPlan() {
        this(Utils.DEFAULT_ENTITY_ID);
    }

    public EPlan(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getYearBegin() {
        return yearBegin;
    }

    public void setYearBegin(int yearBegin) {
        this.yearBegin = yearBegin;
    }

    public int getYearEnd() {
        return yearEnd;
    }

    public void setYearEnd(int yearEnd) {
        this.yearEnd = yearEnd;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    @Override
    public String toString() {
        return "{\"id\":" + id + ";\"yearBegin\":" + yearBegin + ";\"yearEnd\":" + yearEnd + ";\"speciality\":" + speciality + "}";
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, yearBegin, yearEnd, speciality, state);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof EPlan)) {
            return false;
        }
        EPlan other = (EPlan)obj;
        return id == other.id &&
                yearBegin == other.yearBegin &&
                yearEnd == other.yearEnd &&
                speciality == other.speciality &&
                state == other.state;
    }

    public EPlanState getState() {
        return state;
    }

    public void setState(EPlanState state) {
        this.state = state;
    }
}
