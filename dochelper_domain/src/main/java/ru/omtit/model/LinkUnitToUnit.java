package ru.omtit.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "ref_unit_unit")
public class LinkUnitToUnit {
    @Embeddable
    public static class LinkUnitToUnitId implements Serializable {
        @Column(name = "child_id", nullable = false)
        private int childId;
        @Column(name = "parent_id", nullable = false)
        private int parentId;

        public LinkUnitToUnitId() {}

        public int getChildId() {
            return childId;
        }

        public void setChildId(int childId) {
            this.childId = childId;
        }

        public int getParentId() {
            return parentId;
        }

        public void setParentId(int parentId) {
            this.parentId = parentId;
        }

        @Override
        public int hashCode() {
            return Objects.hash(childId, parentId);
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof LinkUnitToUnitId)) {
                return false;
            }
            LinkUnitToUnitId other = (LinkUnitToUnitId) obj;
            return childId == other.childId &&
                    parentId == other.parentId;
        }
    }

    @EmbeddedId
    @JsonIgnore
    private LinkUnitToUnitId key;

    @Column(name = "position")
    @JsonProperty("position")
    private int position;

    @ManyToOne(targetEntity = Unit.class)
    @Transient
    @JsonProperty("child")
    private Unit child;

    public LinkUnitToUnit() {
        this(Utils.DEFAULT_ENTITY_ID, Utils.DEFAULT_ENTITY_ID, 0);
    }

    public LinkUnitToUnit(int childId, int parentId, int position) {
        this.key = new LinkUnitToUnitId();
        this.key.setChildId(childId);
        this.key.setParentId(parentId);
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @JsonProperty("childId")
    public int getChildId() {
        return key.getChildId();
    }

    @JsonProperty("childId")
    public void setChildId(int childId) {
        this.key.setChildId(childId);
    }

    @JsonProperty("parentId")
    public int getParentId() {
        return key.getParentId();
    }

    @JsonProperty("parentId")
    public void setParentId(int parentId) {
        this.key.setParentId(parentId);
    }

    public Unit getChild() {
        return child;
    }

    public void setChild(Unit child) {
        this.child = child;
    }
}
