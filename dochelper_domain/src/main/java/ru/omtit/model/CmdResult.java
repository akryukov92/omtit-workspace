package ru.omtit.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by AKryukov on 09.07.2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CmdResult {

    //private static final Logger logger = Logger.getLogger(CmdResult.class);

    @JsonProperty("message")
    private String _message = "";
    @JsonProperty("result")
    private int _result;
    @JsonProperty("data")
    private Object _data;

    public CmdResult(Object data){
        this("", 0, data);//Success
    }

    public CmdResult(String message, int result){
        this(message, result, null);
    }

    public CmdResult(String message, int result, Object data){
        checkNotNull(message);

        this._message = message;
        this._result = result;
        this._data = data;
    }

    public String getMessage() {
        return _message;
    }

    public int getResult() {
        return _result;
    }

    public void setResult(int value) { _result = value; }

    public Object getData() {
        return _data;
    }
}
