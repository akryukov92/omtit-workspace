package ru.omtit.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "ref_student_group")
public class LinkStudentToGroup {
    @Embeddable
    public static class LinkStudentToGroupId implements Serializable {
        @Column (name = "student_id", nullable = false)
        private int studentId;
        @Column(name = "group_id", nullable = false)
        private int groupId;

        public LinkStudentToGroupId() {}

        public int getStudentId() {
            return studentId;
        }

        public void setStudentId(int studentId) {
            this.studentId = studentId;
        }

        public int getGroupId() {
            return groupId;
        }

        public void setGroupId(int groupId) {
            this.groupId = groupId;
        }

        @Override
        public int hashCode() {
            return Objects.hash(studentId, groupId);
        }

        @Override
        public boolean equals(Object obj) {
            return Objects.equals(studentId, groupId);
        }
    }

    @EmbeddedId
    @JsonIgnore
    private LinkStudentToGroupId key;

    @Column(name = "since")
    @JsonProperty("since")
    @Type(type = "date")
    public Date since;

    @Column(name = "till")
    @JsonProperty("till")
    @Type(type = "date")
    public Date till;

    @Column(name = "order_id")
    @JsonProperty(value = "order_id")
    private String orderId;

    public LinkStudentToGroup() {
        this(Utils.DEFAULT_ENTITY_ID, Utils.DEFAULT_ENTITY_ID, new Date(Calendar.getInstance().getTimeInMillis()), null, "");
    }

    public LinkStudentToGroup(int studentId, int groupId, Date since, Date till, String orderId) {
        this.key = new LinkStudentToGroupId();
        this.key.setStudentId(studentId);
        this.key.setGroupId(groupId);
        this.since = since;
        this.till = till;
        this.orderId = orderId;
    }

    public Date getSince() {
        return since;
    }

    public void setSince(Date since) {
        this.since = since;
    }

    public Date getTill() {
        return till;
    }

    public void setTill(Date till) {
        this.till = till;
    }

    @JsonProperty("studentId")
    public int getStudentId() {
        return key.getStudentId();
    }

    @JsonProperty("studentId")
    public void setStudentId(int studentId) {
        key.setStudentId(studentId);
    }

    @JsonProperty("groupId")
    public int getGroupId() {
        return key.getGroupId();
    }

    @JsonProperty("groupId")
    public void setGroupId(int groupId) {
        key.setGroupId(groupId);
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
}
