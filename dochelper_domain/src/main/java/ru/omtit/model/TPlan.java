package ru.omtit.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "t_plans")
public class TPlan {

    @Embeddable
    public static class TPlanId implements Serializable {
        @Column(name = "unit_id")
        private int disciplineId;

        @Column(name = "e_plan_id")
        private int ePlanId;

        public TPlanId() {}

        public int getEPlanId() {
            return ePlanId;
        }

        public void setEPlanId(int ePlanId) {
            this.ePlanId = ePlanId;
        }

        public int getDisciplineId() {
            return disciplineId;
        }

        public void setDisciplineId(int disciplineId) {
            this.disciplineId = disciplineId;
        }

        @Override
        public boolean equals(Object obj) {
            return Objects.equals(disciplineId, ePlanId);
        }

        @Override
        public int hashCode() {
            return Objects.hash(disciplineId, ePlanId);
        }
    }

    @EmbeddedId
    @JsonIgnore
    private TPlanId key;

    @OneToOne(targetEntity = Unit.class)
    @Transient
    @JsonProperty("discipline")
    private Unit discipline;

    @Column(name = "unit_position")
    @Min(0)
    @JsonProperty(value = "position")
    private int position;

    public TPlan() {
        this.key = new TPlanId();
        this.key.setDisciplineId(Utils.DEFAULT_ENTITY_ID);
        this.key.setEPlanId(Utils.DEFAULT_ENTITY_ID);
    }

    public Unit getDiscipline() {
        return discipline;
    }

    public void setDiscipline(Unit discipline) {
        this.discipline = discipline;
    }

    @JsonProperty("disciplineId")
    public int getDisciplineId() {
        return key.getDisciplineId();
    }

    @JsonProperty("disciplineId")
    public void setDisciplineId(int disciplineId) {
        key.setDisciplineId(disciplineId);
    }

    @JsonProperty("eplanId")
    public int getEPlanId() {
        return key.getEPlanId();
    }

    @JsonProperty("eplanId")
    public void setEPlanId(int ePlanId) {
        key.setEPlanId(ePlanId);
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @JsonIgnore
    public void setKey(TPlanId key) {
        this.key = key;
    }

    @JsonIgnore
    public TPlanId getKey() {
        return key;
    }
}
