package ru.omtit.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Objects;

import static com.google.common.base.Preconditions.checkNotNull;

@Entity
@Table(name = "units")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Unit {
    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "unit_id_seq")
    @SequenceGenerator(name = "unit_id_seq", sequenceName = "unit_id_seq")
    @JsonProperty(value = "id")
    private int id;

    @Column(name = "name")
    @JsonProperty(value = "name")
    private String name = "";

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    @JsonProperty(value = "type")
    private UnitType type;

    @Column(name = "theory")
    @JsonProperty(value = "theory")
    private int theory = 0;

    @Column(name = "practice")
    @JsonProperty(value = "practice")
    private int practice = 0;

    @Column(name = "control")
    @JsonProperty(value = "control")
    private int control = 0;

    @Column(name = "personal")
    @JsonProperty(value = "personal")
    private int personal = 0;

    public Unit() {
        this(Utils.DEFAULT_ENTITY_ID, "", UnitType.UNDEFINED);
    }

    public Unit(int id, UnitType type) {
        this(id, "", type);
    }

    public Unit(UnitType type) {
        this(Utils.DEFAULT_ENTITY_ID, "", type);
    }

    public Unit(int id, String name, UnitType type) {
        checkNotNull(name);
        checkNotNull(type);
        this.id = id;
        this.name = name;
        this.type = type;
    }

    public static CmdResult checkBasics(Unit unit) {
        checkNotNull(unit);
        if (unit.getName().length() > 100) {
            return new CmdResult("Наименование дисциплины должно быть не более 100 символов",1);
        }
        if (unit.getTheory() < 0) {
            return new CmdResult("Количество теоретических занятий не может быть отрицательным",2);
        }
        if (unit.getPractice() < 0) {
            return new CmdResult("Количество практических занятий не может быть отрицательным",3);
        }
        if (unit.getControl() < 0) {
            return new CmdResult("Количество часов на контрольные работы не может быть отрицательным",4);
        }
        if (unit.getPersonal() < 0) {
            return new CmdResult("Количество часов на самостоятельную работу не может быть отрицательным",5);
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        checkNotNull(name);
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UnitType getType() {
        return type;
    }

    public void setType(UnitType type) {
        this.type = type;
    }

    public int getTheory() {
        return theory;
    }

    public void setTheory(int theory) {
        this.theory = theory > 0 ? theory : 0;
    }

    public int getPractice() {
        return practice;
    }

    public void setPractice(int practice) {
        this.practice = practice > 0 ? practice : 0;
    }

    public int getControl() {
        return control;
    }

    public void setControl(int control) {
        this.control = control > 0 ? control : 0;
    }

    public int getPersonal() {
        return personal;
    }

    public void setPersonal(int personal) {
        this.personal = personal > 0 ? personal : 0;
    }

    public int hashCode(){
        return Objects.hash(id);
    }

    public String toString() {
        return "id: " + id + " name: " + name;
    }

    public boolean isGreaterThan(Unit unit) {
        //DISCIPLINE > PART > THEME > ACTIVITY
        if (this.type == UnitType.DISCIPLINE) {
            return unit.type != UnitType.DISCIPLINE;
        }
        if (this.type == UnitType.PART){
            return unit.type == UnitType.THEME;
        }
        if (this.type == UnitType.THEME) {
            return unit.type == UnitType.ACTIVITY;
        }
        if (this.type == UnitType.ACTIVITY) {
            return false;
        }
        return false;
    }
}
