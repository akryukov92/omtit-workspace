package ru.omtit.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import ru.omtit.dao.StringJsonUserType;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.Instant;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "events")
@TypeDefs({
        @TypeDef(name="StringJsonObject", typeClass = StringJsonUserType.class)
})
public class Event {

    @Embeddable
    public static class EventId implements Serializable {
        @Column(name = "unit_id", nullable = false)
        private int unitId;
        @Column(name = "group_id", nullable = false)
        private int groupId;

        public EventId() {}

        public int getUnitId() {
            return unitId;
        }

        public void setUnitId(int unitId) {
            this.unitId = unitId;
        }

        public int getGroupId() {
            return groupId;
        }

        public void setGroupId(int groupId) {
            this.groupId = groupId;
        }

        @Override
        public int hashCode() {
            return Objects.hash(unitId, groupId);
        }
        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof EventId)) {
                return false;
            }
            EventId other = (EventId) obj;
            return unitId == other.unitId &&
                    groupId == other.groupId;
        }
    }

    @EmbeddedId
    @JsonIgnore
    private EventId key;

    @Column(name = "tutor_id")
    @JsonProperty("tutorId")
    private int tutorId;

    @ManyToOne(targetEntity = Tutor.class)
    @Transient
    @JsonIgnore
    private Tutor tutor;

    @Column(name = "place")
    @JsonProperty("place")
    @Size(max = 50)
    private String place;

    @Column(name = "date")
    @JsonProperty("date")
    @Type(type = "date")
    private Date date;

    @Column(name = "marks")
    @JsonProperty("marks")
    @Type(type= "StringJsonObject")
    private String marks;

    @Transient
    @JsonProperty("unit")
    private Unit unit;

    public Event() {
        this(Utils.DEFAULT_ENTITY_ID, Utils.DEFAULT_ENTITY_ID, "", Date.from(Instant.now()), "", null);
    }

    public Event(int unitId, int groupId, String place, Date date, String marks, Unit unit) {
        this.unit = unit;
        this.key = new EventId();
        this.key.setUnitId(unitId);
        this.key.setGroupId(groupId);
        this.place = place;
        this.date = date;
        this.marks = marks;
    }
    
    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getMarks() {
        return marks;
    }

    public void setMarks(String marks) {
        this.marks = marks;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public int getTutorId() {
        return tutorId;
    }

    public void setTutorId(int tutorId) {
        this.tutorId = tutorId;
    }

    public Tutor getTutor() {
        return tutor;
    }

    public void setTutor(Tutor tutor) {
        this.tutor = tutor;
    }

    @JsonProperty("unitId")
    public int getUnitId() {
        return key.getUnitId();
    }

    @JsonProperty("unitId")
    public void setUnitId(int unitId){
        key.setUnitId(unitId);
    }

    @JsonProperty("groupId")
    public int getGroupId() {
        return key.getGroupId();
    }

    @JsonProperty("groupId")
    public void setGroupId(int groupId) {
        key.setGroupId(groupId);
    }
}
