package ru.omtit.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "groups")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Group {
    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "group_id_seq")
    @SequenceGenerator(name = "group_id_seq", sequenceName = "group_id_seq")
    @JsonProperty(value = "id")
    private int id;

    @Column(name = "e_plan_id")
    @JsonProperty("eplanId")
    private int ePlanId;

    @Column(name = "group_name")
    @JsonProperty("name")
    @Size(min = 2, max = 10, message = "Наименование должно быть длиной от 2 до 10 символов")
    private String groupName;

    @ManyToOne(targetEntity = EPlan.class)
    @Transient
    @JsonProperty("eplan")
    private EPlan ePlan;

    public Group() {
        this(Utils.DEFAULT_ENTITY_ID, Utils.DEFAULT_ENTITY_ID, "", null);
    }

    public Group(int id, int ePlanId, String groupName, EPlan ePlan) {
        this.id = id;
        this.ePlanId = ePlanId;
        this.groupName = groupName;
        this.ePlan = ePlan;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getePlanId() {
        return ePlanId;
    }

    public void setePlanId(int ePlanId) {
        this.ePlanId = ePlanId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public EPlan getEPlan() {
        return ePlan;
    }

    public void setEPlan(EPlan ePlan) {
        this.ePlan = ePlan;
    }
}
