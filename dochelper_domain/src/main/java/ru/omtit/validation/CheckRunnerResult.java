package ru.omtit.validation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by AKryukov on 31.07.2015.
 */
public class CheckRunnerResult {

    List<String> messages;
    boolean success;

    public CheckRunnerResult() {
        messages = new ArrayList<>();
    }

    public boolean isSuccess() {
        return success;
    }

    public List<String> getMessages() {
        return messages;
    }
}
