package ru.omtit.validation;

import java.util.function.Function;

/**
 * Created by AKryukov on 31.07.2015.
 */
public class ValidationCheck<T> {
    private Function<T, Boolean> fx;
    private String message;

    public ValidationCheck(Function<T,Boolean> fx, String message) {
        this.fx = fx;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
    public Function<T, Boolean> getFunction() { return fx; }
}
