package ru.omtit.validation;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

/**
 * Created by AKryukov on 31.07.2015.
 */
public class CheckRunner<T> {

    private List<ValidationCheck<T>> checks;

    public CheckRunner(){
        checks = new ArrayList<>();
    }

    public void add(Function<T,Boolean> fx, String message) {
        checks.add(new ValidationCheck<T>(fx, message));
    }

    public CheckRunnerResult check(T obj) {
        CheckRunnerResult ret = new CheckRunnerResult();
        ret.success = true;
        for (ValidationCheck check : checks) {
            Function<T, Boolean> fx = check.getFunction();
            Boolean passed = fx.apply(obj);
            if (passed) {
                ret.messages.add(check.getMessage());
                ret.success = false;
            }
        }
        return ret;
    }
}
