package ru.omtit.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

/**
 * Created by AKryukov on 28.07.2015.
 */
public class TPlanTest {
    private ObjectMapper mapper;

    @Before
    public void setUp() {mapper = new ObjectMapper();}

    @Test
    public void testCanReadAddJson() throws IOException {
    }

    @Test
    @Ignore
    public void testJacksonSerialize() throws IOException{
        String input = "{\"discipline\":{\"id\":17,\"name\":\"d1\",\"type\":\"DISCIPLINE\",\"theory\":\"1\",\"practice\":\"2\",\"personal\":\"3\"},\"eplanId\":3200,\"position\":0}";
        TPlan result = mapper.readValue(input, TPlan.class);
        assertEquals(3200, result.getEPlanId());
        assertEquals(17, result.getDiscipline().getId());
    }

    @Test
    @Ignore
    public void testJacksonSerialize2() throws IOException{
        String input = "{\"disciplineId\":100,\"eplanId\":46,\"discipline\":{\"name\":\"test3\",\"id\":100,\"type\":\"DISCIPLINE\",\"theory\":3,\"practice\":5,\"personal\":7},\"position\":0}";
        TPlan result = mapper.readValue(input, TPlan.class);
        assertEquals(100, result.getDiscipline().getId());
        assertEquals(46, result.getEPlanId());
    }
}
