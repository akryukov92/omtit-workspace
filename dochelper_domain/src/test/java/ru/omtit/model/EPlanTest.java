package ru.omtit.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;


/**
 * Created by AKryukov on 23.07.2015.
 */
public class EPlanTest {

    private ObjectMapper mapper;

    @Before
    public void setUp() {
        mapper = new ObjectMapper();
    }

    @Test
    @Ignore
    public void testIgnoresUnspecifiedParams() throws IOException {
        String input = "{\"id\": 1, \"yearBegin\": 2014, \"yearEnd\": 2015, \"speciality\": 230105, \"disciplines\": []}";
        EPlan result = mapper.readValue(input, EPlan.class);
        assertEquals(1, result.getId());
        assertEquals(2014, result.getYearBegin());
        assertEquals(2015, result.getYearEnd());
        assertEquals("230105", result.getSpeciality());
    }

    @Test
    @Ignore
    public void testCanReadAddJson() throws IOException {
        String input = "{\"id\": -1, \"yearBegin\": 2014, \"yearEnd\": 2015, \"speciality\": 230105}";
        EPlan result = mapper.readValue(input, EPlan.class);
        assertEquals(-1, result.getId());
        assertEquals(2014, result.getYearBegin());
        assertEquals(2015, result.getYearEnd());
        assertEquals("230105", result.getSpeciality());
    }

    @Test
    @Ignore
    public void testCanReadUpdateJson() throws IOException {
        String input = "{\"id\":3, \"yearBegin\":2016, \"yearEnd\":2017, \"speciality\": 230105, \"tPlans\":[]}";
        EPlan result = mapper.readValue(input, EPlan.class);
        assertEquals(3, result.getId());
        assertEquals(2016, result.getYearBegin());
        assertEquals(2017, result.getYearEnd());
        assertEquals("230105", result.getSpeciality());
    }

    @Test
    @Ignore
    public void testJacksonSerialize3() throws IOException {
        String input = "{\"id\": 1, \"yearBegin\":2015,\"yearEnd\":2016, \"speciality\":230105, \"editing\":true}";
        EPlan result = mapper.readValue(input, EPlan.class);
        assertNotEquals(result.getId(), Utils.DEFAULT_ENTITY_ID);
    }

    @Test
    @Ignore
    public void testJacksonSerialize4() throws IOException {
        String input = "{\"id\":29,\"yearBegin\":0,\"yearEnd\":0,\"speciality\":null,\"state\":null}";
        EPlan result = mapper.readValue(input, EPlan.class);
        assertEquals(result.getId(), 29);
    }

    @Test
    @Ignore
    public void testJacksonSerialize5() throws IOException {
        EPlan input = new EPlan(31);
        String value = mapper.writeValueAsString(input);
        EPlan output = mapper.readValue(value, EPlan.class);
        assertEquals(output, input);
    }
}
