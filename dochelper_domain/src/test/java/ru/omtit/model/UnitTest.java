package ru.omtit.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;
/**
 * Created by AKryukov on 23.07.2015.
 */
public class UnitTest {

    private ObjectMapper mapper;

    @Before
    public void setUp() {
        mapper = new ObjectMapper();
    }

    @Test
    @Ignore
    public void testCanReadSaveNoChildrenJson() throws IOException {
        String input = "{\"id\":-1, \"name\":\"test name\", \"type\":\"DISCIPLINE\", \"theory\": 3, \"practice\": 5, \"control\": 7, \"personal\": 11}";
        Unit result = mapper.readValue(input, Unit.class);
        assertEquals(-1, result.getId());
        assertEquals("test name", result.getName());
        assertEquals(UnitType.DISCIPLINE, result.getType());
        assertEquals(3, result.getTheory());
        assertEquals(5, result.getPractice());
        assertEquals(7, result.getControl());
        assertEquals(11, result.getPersonal());
    }

    @Test
    @Ignore
    public void testParseList() throws IOException {
        String input="[]";
        List units = mapper.readValue(input, List.class);
        assertTrue(units.isEmpty());
    }

    @Test
    @Ignore
    public void testJacksonSerialize() throws IOException {
        String input = "{\"id\": 5, \"name\": \"\", \"position\": 0, \"type\": \"DISCIPLINE\", \"theory\": 0, \"practice\": 0, \"control\": 0, \"personal\": 0}";
        Unit result = mapper.readValue(input, Unit.class);
        assertNotEquals(result.getId(), Utils.DEFAULT_ENTITY_ID);
    }

    @Test
    @Ignore
    public void testJacksonSerializeLink() throws IOException {
        String input = "{\"child\":{\"id\":7,\"name\":\"Блочная верстка\",\"position\":0,\"type\":\"PART\",\"theory\":\"1\",\"practice\":\"3\",\"personal\":\"0\"},\"parentId\":7350,\"position\":0}";
        LinkUnitToUnit link = mapper.readValue(input, LinkUnitToUnit.class);
        assertNotNull(link.getChild());
    }
}
